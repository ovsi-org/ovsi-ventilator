# Glossary of terms

- Last updated: 08/07/2021
- Currently hosted externally on Airtable at [OVSI Ventilator](https://airtable.com/shrNytGhFODpsv5fr/tblvP1zU3ShEVCXxy)  
Add:  
- Inspiratory-expiratory ratio (I:E)  
- Pressure Controlled Ventilation (PCV)  
- Continuous Mandatory Ventilation (CMV)

| Term | Definition | Source |
| ------ | ------ | ------ |
| Compressible volume | The internal volume of the device and the added mechanical dead space that the use of the device will add to the breathing system. | https://www.intersurgical.com/info/filtrationandhumidificationglossary |
| Continuous positive airway pressure (CPAP) | A form of positive airway pressure ventilator that supplies continuous pressure to keep the airways open in patients who cannot breathe spontaneously. | https://www.intersurgical.com/info/filtrationandhumidificationglossary |
| Mechanical dead space | The compressible/internal volume of devices added to the breathing systems which results in an increase in the volume of the system that is not involved in gas exchange. | https://www.intersurgical.com/info/filtrationandhumidificationglossary |
| PEEP | Abbreviation for positive end-expiratory pressure. Ventilation in which airway pressure is maintained above atmospheric pressure at the end of exhalation by means of a mechanical impedance, usually a valve, within the circuit. | https://www.intersurgical.com/info/filtrationandhumidificationglossary |
| Resistance to flow | Airway resistance of the respiratory tract to inhalation and expiration. This is an expression of the amount of effort that is required to make an inspiratory or an expiratory breath. | https://www.intersurgical.com/info/filtrationandhumidificationglossary |
| Tidal volume | The volume of gas inhaled and exhaled by the patient during one respiratory cycle. The average for a 70 Kg or 155 lbs adult is 500 ml. | https://www.intersurgical.com/info/filtrationandhumidificationglossary |
