# The prototype design

## Disclaimer 
This design is not yet ready to me distributed. Many parts of the design do not comply to safety requirements. Check the 'parts of the design that needs changing' section of the readmes for each components to learn more.

This project will also need calibration before it can be safely used.

- [ ] This readme should include accurate description of the tests performed and results obtained for this design.

## System overview

- Version: 3

![](System_Overview_3.png)

## Aero-mechanical layout

- Piping specification
- Version: 3.5
- Source: https://docs.google.com/drawings/d/1-Kx0M1PGWQ4sOfdMsjO4XJH6aazlhqHNkjtaziJQzOs/edit

![](Piping_Spec_Parts_v3.5.jpg)

1. [Viral filter](Components/ViralFilter)
2. Fitting for box (intake)
3. [Dust filter](Components/DustFilter)
4. [Compressor inlet](Components/Compressor)
5. [Compressor + volute](Components/Compressor)
6. [Pressure relief valve](Components/PressureReliefValve)
7. [Throttle valve](Components/ThrottleValve) - [ ] **Shouldn't there be two of these?**
8. [Check valve](Components/CheckValve)
9. [V-Flow meter - oxygen](Components/VFlowMeter)
10. [Check valve](Components/CheckValve)
11. [Pilot pressure operated regulator](Components/PilotPressureOperatedRegulator) - [ ] **Not labelled on diagram**
12. Oxygen pressure regulator
13. Fitting for box (oxygen)
14. [Mixing tank](Components/MixingTank)
15. [Safety relief valve](Components/SafetyReliefValve)
16. [Inhale solenoid valve](Components/InhaleSolenoidValve) (Normally Closed)
17. [Check valve](Components/CheckValve)
18. [V-flow meter - air-oxygen mix](Components/VFlowMeter)
19. [Check valve](Components/CheckValve)
20. Fitting for box (to patient)
21. [Viral filter + Heat and Moisture Exchanger](Components/ViralFilter)
22. [Viral filter](Components/ViralFilter)
23. Fitting for box (from patient)
24. [Viral filter](Components/ViralFilter)
25. [Exhale solenoid valve](Components/ExhaleSolenoidValve) (Normally Open)
26. Fitting for box (PEEP)
27. [PEEP valve](Components/PEEPValve) (optional)
28. Fitting for box - [ ] **Not labelled on diagram**
29. Casing - [ ] **Not labelled on diagram**
30. Vent box

There are also a number of [elbow and tee connectors](Components/Connectors) used.

Many of the parts are listed in this [bill of materials spreadsheet](BOM.xls). **The numbers do not match with the diagram. ** This was taken from `BOM_SA_v2.0.xlsx`

A STEP file of the is available in the file [`W3.5_assembly_asm.STEP`](W3.5_assembly_asm.STEP) **Where is the original CAD file?**

## Electronics and control layout

The key function of the ventilator is to provide the patient with oxygen-enhanced air at specific times and durations.  The electronic control circuit is designed to perform this task by sensing pressure of the inhale and exhale lines and actuating an air motor and two solenoid control valves.  The other areas within the system are a touch input enabled graphic display, alarm buzzers, temperature sensors, ambient environment sensor and various DC regulation and conversion elements.

![](Electronics_System_Diagram.png)

*Fig. 3. General block diagram showing the architecture of the ventilator. The CPU is based on a STMicroelectronics STM 32 F103 microcontroller.  T1, T2,T3 are temperature sensors, P1, P2, P3, P4, P5 are pressure transducers.  The graphical display is a 7” colour system with touch input.*

## Firmware

**Where is the firmware?**

## Glossary

See glossary of technical terms in [glossary](Glossary).
