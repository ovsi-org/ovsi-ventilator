# Power Management system

![](power_labelled_3.png)  
  Fig.1: A general schematic, visualising the electronics design and the connections between DC converter units in the power management system
![](power_labelled_2.PNG)  
  Fig.2: Protection circuitry added to each section of the power management system

The power management system contains a medically approved AC to DC adaptor, which takes ¬240Vrms from the main and converts it to 12V DC output (red circle). The 12V are fed into the main source input of TPS2120 power multiplexer board, which compares the 12V output to the output from a Li/Ion backup battery (blue square). If there is a power outage, unstable power supply or too much ripple at the DC output, the TPS2120 will switch over to the secondary power source: the Li/Ion battery. The board has been programmed to automatic switching, meaning that the switching happens immediately and is not controlled to the CPP but an alarm system provides feedback externally to the clinician. Once the mains is reapplied, the board switches back to the primary power source, and the battery gets charged until it reaches its maximum capacity. Protection circuitry is used at the inputs of the TPS2120 as shown on Fig.1 to ensure the safety of the board since it can withstand only 3-4A.

The protection circuitry (Fig.2) contains a fuse, protecting the circuit from currents above 6.3A, the unidirectional diode protects the circuit from overvoltage and the zener diode protects the circuit from overvoltage above 14V. A capacitor is used in parallel to the zener in order to store charge and regulate the switching in case of power outrage. As seen from the video, when the board switches between the power sources, the LED at the output does not even blink since the capacitor stores the charge until the board switches to the secondary source, providing seamless switchover. If the system switched to the Li/Ion battery and supplies only 11.1V instead of 12V, this is not a problem since the rest of the system contains DC converters which take an input as low as 8V.

The 12V output from the TPS2120 board is fed into more protection circuitry before it reaches four medically approved DC-to-DC converters: two 5V DC/DC converters and two 3.3V DC/DC converters. Each pair of DC converters is connected to a TPS2120 board, similarly to the AC supply and the backup battery. The redundacy is inplemented so that if one of the components from the protection circuitry fails or there is a fault within one of the DC converter, the board will switch to the secondary DC converter. LEDs are connected at the output of the DC converters before the input of the power multiplexer so that the fault could easily be recognised and located. In that way, there is feedback to the clinician and the CPU, informing them whether the fault is within the switching board or the DC converter units. The 5V output of the TPS2120 boards powers up the digital differential pressure sensors and the display, while the 3.3V powers up the CPU. The full prototype is shown on Fig.3.

![](power_labelled.PNG)

In order to manage the battery, an external battery management system (BMS) was connected to the Li/Ion battery.The BMS is connected to the AC and the battery will start recharging if it was used before. Usually, BMS system should be able to recharge the system fairly quickly but with some limitations since fast charging causes overheating and the system cannot heat over 43oC degrees according to IEC60601. Secondly, the BMS should be able to monitor temperature, current and voltage in order to be able to estimate capacity and ensure safe recharge. Finally, the BMS should have internal self-protection features to align with all regulations and ensure the safety of the users. Moreover, it's good to have a direct feedback from the BMS to the CPU. In order to incorporate all these features, without having to create a custom PCB design, which is hard to source and fix in case of a components failure, development boards have been use. On Fig.3 is shown one of the prototypes, using a board, which incorporates all features into one board, however, we have shown other options such as having separate development boards for charging of multi-cell batteries and for monitoring and protecting the battery system. 

The system has considered some inclusive innovation aspects such as the possibility of a very unstable power supply, which is why TPS2120 is chosen to switch automatically as many times as needed and to be able to switch back when a stable power source is reapplied. Another consideration is that the power system should contain an internal electrical power source capable of powering the ventilator for at least 30 mins when the supply mains fails, however, the design of the system allows to connect a battery which can power up the system for longer periods of time, especially if the OVSI ventilator is required in desert areas. The design of the power system looks complex but consists of easily sourceable components which can be substituted any time with similar components if needed. The only component that might not be easily sourceable is the TPS2120 but similar power multiplexers are widely available and could be easily implemented in the design. 


## Further testing to be performed
   -Testing at what specific voltage the TPS2120 board switched to the 11.1V Li/Ion battery  
   -At what ripple amplitude, the TPS2120 board switched to the 11.1V Li/Ion battery  

## Medical Regulations Requirements
Alongside the main medical device standard (BS EN 60601-1:2006+A12:2014), additional requirements specifically for ventilators are described within BS EN ISO 80601‑2‑12:2020 (as of 2nd August 2021, still available to view online for free https://www.iso.org/obp/ui#iso:std:iso:80601:-2-12:ed-2:v1:en). 

Particular standards relating to power supply and battery management include:
**201.11.8.101 Additional requirements for interruption of the power
supply/supply mains to ME equipment** - The ventilator must include an internal power source which is capable of powering the ventilator for at least 30 minutes in the event of mains power supply failure. There also needs to be a means to measure the remaining capacity or operating time provided by the internal power source. The system must be able to determine which power source is currently powering the device at any given time. An alarm system must also be capable of detecting power switchover and alert the user of remaining operating time. Vigorous testing is also required to characterise ventilator behaviour during the switching states to ensure operation remains safe.

### Regulations addressed by the design with respect to IEC 60601/80601: 

| Regulations taken from IEC 60601/80601 | Design implementations |
|---|---| 
| Internal electrical power source capable of powering the ventilator for at least 30 mins when the supply mains fails| Incorporated a Li/Ion light and portable backup battery. Using TPS2120 seamless switchover power multiplexer board, the system will switch automatically from the AC power source to the backup battery in case of power shutdown. Once the power is reapplied, the system will switch back to the AC power and the battery will be recharged.|
| Means of determining which power source is currently powering the system and the state of the internal power supply in case of mains shutdown | Implementing an alarm system, described in the alarm section, which will inform the clinician when the system switched to the battery. Using a complex battery management system with voltage and current sensors to monitor the battery capacity at any given time| 
|Near depletion and at least 5 mins prior to loss of ventilation, an alarm should inform the clinician of the remaining capacity of the internal power source | Implementing hardware battery system which provides feedback to the clinician as battery capacity decreases|
|Battery system that does not overheat (at any given time < 43oC), does not surpass max charging voltage, and performs cell balancing for safe charging| The battery management system incorporates self-protection features, cell balancing, battery monitoring features (Current, Voltage, Temperature measurements which are sent also to the CPU)|
| Ensure safe I/V readings with less than 10 % peak to peak ripples (Overvoltage, Overcurrent, Surge Voltage | Introducing protection circuitry to ensure safety and stability (shown on Fig.2). 
| Comply with supply regulations (e.g internal resistance, ripple; max I/V readings)| Incorporating medically approved AC/DC converter and DC/DC converters
| Exhaustive testing must be performed and the system should remain functional for as long as possible to ensure the reliability of the ventilator| Introducing redudancy by using 2xDC converter circuitry units to ensure reliability in case a component from one of the units fails
| An alarm system to signal any power failure| Introducing a complex alarm system as described in the Alarm section as well as providing feedback with LEDs from any DC converter circuitry unit to be able to recognise quickly where a fault is coming from| 

## To do list

- [ ] Quick walkthrough the different files and what they do with appropriate links.
 - The README.md file contains the general user-friendly schematic and covers the functionality and the features of the power management system
 - The Prototype.md file contains more detailed explanation of the electronics in the power management system and contains schematics, showing the components of the prototype
- [ ] Add a excel document with the list of all the parts required
## References
ISO 80601-2-12:2020 Medical electrical equipment — Part 2-12: Particular requirements for basic safety and essential performance of critical care ventilators

