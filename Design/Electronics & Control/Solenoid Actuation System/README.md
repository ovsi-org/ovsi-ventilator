# Solenoid actuation system

[[_TOC_]]

## 1. Overall

There are inhale and exhale valves within the ventilator to achieve enhanced patient breathing this.  The inhale valve is normally closed (NC) and the exhale valve is normally open (NO).  This is one of the key functions of the control electronics within the device.  

The CPU controls the SAS in order to switch the valve states.  This is via solenoid actuators. Taking a single SAS, this is performed by the CPU outputting two digital signals that are received by the SAS.  Via a MOSFET-based circuit, this allows states a three state solenoid activation to be performed.  Firstly a high state (40W) is achieved by the circuit, causing the physical displacement of the solenoid and changing the valve stage.  Secondly, a mid state (10W) is achieved where the solenoid is held in position.  Finally a zero state is achieved where the valve returns to its normal state.

### 1.1 Connections

*List connections to solenoid actuation system*

## 2. Design

### 2.1 Schematic 

![](SolenoidActuationSystem.png)

*Fig.1 It can be seen that depending on the values of digital outputs from the CPU for High and Hold the current passing through the solenoid inductor will be changed. The the main SAS is separated from the CPU by an optocoupler to prevent unwanted issues.*
