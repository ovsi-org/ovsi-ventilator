# Alarm system overview
In order to fit the regulations [^1], the OVSI system needs to have a set of alarm systems to warn the users of possible issues with the system.

Each alarm comes with its specific condition and priority level.
High priority alarm are accompanied with description of possible causes and actions to take (ex. obstruction, check for blockages). Audio pause can’t exceed 120s without intervention. This requires an alarm log system with a capacity of 1000 different events.with the following requirements:
* high and medium priority alarms and alarm inactivations records.
* Time stamps for each event.
* Must be kept even if the power is lost for 7 days.

The log is also needed for any change in alarm or ventilator settings, new patient, power change and results of pre-use checks.

# Alarm requirements [1]
1. Oxygen level 
    * High oxygen level alarm **medium priority**
2. Airway pressure
    * Max limited pressure 20hPa above alarm limit or 125hPa 
    * High pressure level alarm **high priority**
    * Set or related to set pressure of ventilator
    * If operator can set then confirmation routine is needed  
    * Transient increase due to coughing should not set off alarm 
    * Delays not more than 200ms 
    * Ventilator should act to reduce pressure 
    * Within two respiratory cycles or 15s pressure should be atm or BAP 
3. Disconnect **Medium priority** + Cant turn alarms off for a ventilator dependent patient.
    * Pressure at ventilator gas output port
    * Flow at exhalation valve
    * Expired flow pressure
    * End tidal co2
4. Expired volume ?
    * Low limit **medium priority** or could escalate from low to medium 
    * High limit **medium priority**
Limits can be pre-adjusted, ventilator or operator adjustable. If ventilator is adjustable, then an algorithm is needed.
5. Power source
    * Switchover **low priority**
    * Internal 10 mins **medium priority**
    * Internal 20 mins **high priority**
6. Gas supply failure
    * Maintain normal use if one gas supply fails 
    * Detect when one gas supply fails **low priority**
7. Obstruction
    * Blocked breathing tubes, filters or valves  **High priority**
    * Delay not longer than 2 respiratory cycles or 5s 
    * Within one respiratory cycle pressure should be atm or BAP 
8. PEEP (Positive end expiratory pressure)
    * High peep alarm required **medium priority**
    * Low peep alarm desirable **medium priority**
    * Delay not longer than 3 inflations
9. Failure of connection to control or monitoring **medium priority**
10. Apnoea  
    * Related to expiration time 

 The alarm requirements can be configured into application alarms, and technical alarms, as outlined in the table below [2]. The tones and visual indications are taken from the Hamilton Medical ventilator. This table also begins to consider what physical connections need to be made for each of the alarms in the hardware system. This allowed judgement of which alarms would be feasible in the given time and with the Whittle 2 prototype that was available. 


| Alarm Name            | Type of alarm  | Visual         | Tones                 | Priority                         | Detection Condition                                                                                                                                            | System connection                                                                                                         |
|-----------------------|----------------|----------------|-----------------------|----------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| PIP                   | Application    | Red            | 5 beeps               | High                             | input pressure                                                                                                                                                 | input pressure via CPU                                                                                                    |
| PEEP                  | Application    | Yellow         | 3 beeps               | Medium                           | expired pressure 10cmh20 above or 5cm h20 below set value                                                                                                      | expired pressure via CPU                                                                                                  |
| Disconnect            | Application    | Yellow         | 3 beeps               | Medium                           | expired pressure drops to 0 or very low                                                                                                                        | expired pressure via CPU                                                                                                  |
| Expired tidal volume  | Application    | Yellow         | 3 beeps               | Medium                           | 100 to 150ml more or less than expected                                                                                                                        | tidal volume calculated from expired pressure via CPU. Need to consider the compliance of the tubes to the ventilator     |
| Obstruction           | Application    | Red            | 5 beeps               | High                             | Calculated from airway resistance and various pressures                                                                                                        | expired and inhaled pressure via CPU.                                                                                     |
| Aponea                | Application    | Red            | 5 beeps               | High                             | Low respiratory rate. 10 to 15 bpm less than the expected total rate. could also just be included in resp rate. patient triggering via input pressure sensor.  | Input pressure triggering calculation of respiration rate. Could also be used for high and low respiration rate equations |
|                       |                |                |                       |                                  |                                                                                                                                                                |                                                                                                                           |
| Gas supply failure    | Technical      | Yellow         | 2 beeps, not repeated | Low                              | Gas supply pressure falls to close to 0                                                                                                                     | pressure sensor after compressor for oxygen, pressure sensor at inhale for air                                        |
| CPU failure           | Technical      | Red            | 5 beeps               | High                             | no power to p-channel mosfet                                                                                                                                   | output from cpu to p channel mosfet, when signal is 0, power is given to led and buzzer                                   |
| Internal battery low  | Technical      | Red and Yellow | 5 beeps               | 10 mins = High 20 mins = medium  | no power to high-side ic                                                                                                                                       | output from connection to high side ic, when signal is 0, power is given to led and buzzer                                |
| Alarm Battery Failure | Technical      | Yellow         | 3 beeps               | Medium                           | no power to high-side ic                                                                                                                                       | output from connection to high side ic, when signal is 0, power is given to led and buzzer                                |
| Power switchover      | Technical      | Yellow         | 2 beeps               | Low                              | no power to high-side ic                                                                                                                                       | output from connection to high side ic, when signal is 0, power is given to led and buzzer                                |
| DC/DC converter error | Technical      | Yellow         | 2 beeps               | Low                              | no power to p-channel mosfet                                                                                                                                   | output from connection to p channel mosfet, when signal is 0, power is given to led and buzzer                            |


# Alarm high level design
Only the technical alarms were able to be implemented in the current Whittle 2 system. These technical alarms were designed, simulated using LTSpice and then prototyped in the lab using breadboards. The next would be to design a PCB for the alarm system.

Two different circuits were included in the system:
1. A 555 based circuit to monitor the state of the connected battery. 
2. A p-channel MOSFET based circuit to monitor that components are connected and producing an output.

Figure 1 is the overall schematic of the system. 

![](alarmschematic.png)
Figure 1 

# 555 based alarm circuit design
The function of this circuit is to trigger the alarm when the battery capacity reaches a set point. The set point can be configured by altering the resistors in the circuit. In this set-up the set point is around 10V, because the internal battery is around 11.1V. Lithium-ion batteries do not dishcharge 0V even when empty. 

Figure 2 and figure 3 show the circuit diagram and the voltage across the LED for various input voltages respectively. 

![](circuit__29_.png)
Figure 2 

![](555newsim.png)
Figure 3

The component list is as follows:
* 22kΩ resistor
* 8.2kΩ resistor x2
* 680Ω resistor
* 10nF capacitor
* 8 pin SE555P timer circut
* 4.7V zenner diode (1N5337BG)
* 1.8-2V LED
* 11.1V internal battery 
* 4.8V alarm battery

# P-channel MOSFET based alarm circuit design 
The function of this circuit is to trigger the alarm when the voltage output from a component reaches near to or 0V. It relies on the p-channel MOSFET to trigger the alarm when the input is low. 

Figure 4 and figure 5 show the circuit diagram and the voltage across the LED for various input voltages respectively. 

![](circuit__31_.png)
Figure 4

![](newsim.png)
Figure 5

The component list is as follows:
* 70kΩ resistor
* 10kΩ resistor
* 680 resistor
* 12V p-channel MOSFET
* 14V zenner diode
* 1.8-2V LED
* 5V voltage source (from power management system)

# Prototyping
The specific alarms that were able to be implemted are:
 * Internal battery capacity (555)
 * Power switching from mains power to internal batter (p-channel MOSFET)
 * Switching between DC/DC converters x 2 (p-channel MOSFET)
 * Checking that the alarm battery is connected (p-channel MOSFET)

The alarm system was connected to the power management system and the components were tested individually. In figure 6, the p-channel MOSFET circuits are shown in blue and the 555 timer circuits in yellow. 

![](alarmprototype.png)
Figure 6

# Next steps
1. Finish prototyping technical alarm system.
   * Need to include addition 555 circuit for monitoring state of alarm battery. 
   * Need to include buzzer and temporary switch for combating alarm fatigue. 
   * Need to configure a 5V alarm battery. 
2. Work on application alarms.
   * Work out how to incorporate sensors into flow hardware.
   * Work out how to connect CPU to sensors.
   * Write code for sensors.
3. Connect alarm system to the display.
4. Design PCB for entire alarm system. 
   

# References
[1]: BS EN ISO 80901-2-12:2020 Medical electrical equipment — Part 2-12: Particular requirements for basic safety and essential performance of critical care ventilators. 
[2]:  B. A. Moore and J. E. Barnett,  “Oxford clinical psychology military psychologists ’ desk reference,”Case  Studies  in  Clinical  Psychological  Science:  Bridging  the  Gap  from  Science  to  Practice,  pp.  1–7,2015.
