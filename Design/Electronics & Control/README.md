# Electronics and control systems [THIS WHOLE SECTION WILL HAVE TO BE REDONE BY CDT STUDENTS]
This section has four main sub-folders:  
[Power Supply management](https://gitlab.com/ovsi-org/ovsi-ventilator/-/tree/master/Design/Electronics%20%26%20Control/Power%20management%20system)  
[Pressure Sensor System](https://gitlab.com/ovsi-org/ovsi-ventilator/-/tree/master/Design/Electronics%20&%20Control/Pressure%20Sensor%20System)  
[Solenoid Actuation System](https://gitlab.com/ovsi-org/ovsi-ventilator/-/tree/master/Design/Electronics%20&%20Control/Solenoid%20Actuation%20System)  
[User Interface](https://gitlab.com/ovsi-org/ovsi-ventilator/-/tree/master/Design/Electronics%20&%20Control/User%20Interface)
## Electronics system diagram

![](Electronics_System_Diagram.png)

## Aeromechanical and electronic integration diagram

![](Aeromechanical_Electronic_Integration.png)

## Electrical layout

![](Electrical_Layout.png)
