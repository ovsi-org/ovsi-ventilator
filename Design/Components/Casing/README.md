# Casing

[[_TOC_]]

The requirement for the ventilator to be able to travel large distances without damage drives
the packaging towards a robust, hard wearing case. Additionally it must provide at least IP22
protection against ingress of water. As such a modified Peli Case is used to house the
ventilator.

The case is altered to include four adapters for gas inflow/outflow (Oxygen in, air to Patient, air from Patient and exhaust), access to valve control handles and housing for a 7” touch screen display.

## Specifications

## Purchasing

### Case

### Vent

## References

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
