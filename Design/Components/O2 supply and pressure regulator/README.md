# O2 supply and Pressure regulator
Those two components are external units from the original ventilator. The O2 supply can come from a bottle of O2 or an oxygen concentrator (1), and the pressure delivered to the system is modulated by the Manually adjustable Pressure regulator (2).

![O2 supply and Manually Adjustable Pressure Regulator in the design](O2_pressure_chart.PNG "O2 supply and Manually Adjustable Pressure Regulator in the design")

Both these components are Off the Shelf, but their possible risks and failures are described [here](Risks&Failures_O2.md) for the O2 supply, and [here](Risks&Failures_mapr.md) for the manually adjustable pressure regulator. 

## O2 supply
### Specifications? 
### Purchase informations

## Manually adjustable Pressure regulator
### Specifications? 
### Parts of the design that need attention 
- [ ] Safety valve 2 needs to be sized to deal with the O2 supply  without pressure regulator i.e. if the pressure regulator breaks, pressure switch will break and safety valve 2 needs to be sized to deal with the oxygen flow at the O2 supply pressure and flow rate.
- [ ] check all O2 bottles have filter, if not, explore option of introudcing a filter to design. Add label to ensure filter is used on inlet.
### Purchase informations

