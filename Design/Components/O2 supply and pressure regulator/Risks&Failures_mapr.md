## Risks and Failures
This document shows the possible risks and failures of the Manually Adjustable Pressure Regulator situated between the O2 supply and the OVSI system. 

### Fails to regulate pressure down to correct value, delivers oxygen at too pressure above specified inlet pressure
**Effects: breaking of pressure switch, which leads to wasting O2 through safety valve, oxygen fraction too high. Safety relief valve 2 regulates pressure preventing overpressurisation of system**
| Causes | Controls |
|---|---| 
| corrosion | oxygen compatible medical grade of the shelf component, high oxygen alarm triggered by O2 sensor, pressure switch is still functional and stops flow of O2 when pressure drops below 3.5kPa, if the pilot operated pressure regulator breaks due to high O2 pressure, safety valve #2 vents the O2 and keeps the system at the upper pressure limit. This is likely to be above the patient inhalation pressure, so the high pressure alarm will sound. |
| knock or drop of ventilator breaks pressure regulator | ventilator designed for 1.5m fall, high oxygen alarm triggered by O2 sensor |
| foreign object ingestion | inlet and viral filters upstream, high oxygen alarm, O2 bottles have filter |

- [ ] Safety valve 2 needs to be sized to deal with the O2 supply  without pressure regulator i.e. if the pressure regulator breaks, pressure switch will break and safety valve 2 needs to be sized to deal with the oxygen flow at the O2 supply pressure and flow rate.
- [ ] check all O2 bottles have filter, if not, explore option of introudcing a filter to design. Add label to ensure filter is used on inlet.

### Delivers oxygen at too low a pressure, below specified inlet pressure
**Effects: only air can be delivered to patient. If the resulting pressure at the junction of air/oxygen circuits is 
lower than pressure relief threshold, delivery flowrate will be reduced below desired value.**
| Causes | Controls |
|---|---| 
| foreign object ingestion leads to regulator blockage | inlet and viral filters upstream, oxygen alarm triggered by O2 sensor |
| corrosion stops regulator opening | oxygen alarm triggered by O2 sensor, oxygen compatible medical grade of the shelf component |

### leaks to outside main gas path
**Effects: wasting oxygen outside of box. no depresurisation of system because pilot operated pressure regulator isolates ventilator from leak**
| Causes | Controls |
|---|---| 
| drop or knock | low oxygen alarm , potential low pressure alarm |

## References
FMEA paper from OVSI team
