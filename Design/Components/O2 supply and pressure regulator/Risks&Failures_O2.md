## Risks and Failures
This document shows the possible risks and failures of the O2 supply. 

### Fails to supply O2
**Effect: Patient recieves only air without additional oxygen**
| Causes | Controls |
|---|---| 
| External oxygen reservoir empty | low oxygen alarm, triggered by O2 sensors to sound when supply is getting low, oxygen flow meter reading falls to zero |
| O2 delivery pressure drops below system pressure | low oxygen alarm triggered by O2 sensor, pressure gauge on pressure regulator, oxygen flow meter reading falls to zero |
| wall supply cut off | low oxygen alarm, triggered by O2 sensors to sound when supply is getting low, oxygen flow meter reading falls to zero |

## References
FMEA paper from OVSI team

