## Risks and Failures
This document shows the possible risks and failures of the Mixing Tank
### Ruptures
**Effects: Depressurisation of the system**
| Causes | Controls |
|---|---| 
| Damage due to knock or drop. | Shock absorbant case; and foam packed around components.  Low inhalation pressure alarm. Low tidal volume alarm. |
| Overpressure |  Pressure relief valve and safety relief valves. High inhalation pressure alarm. |
| Debris in airstream. | Inlet filter. Low tidal volume alarm. Low inhalation pressure alarm. |

### Leaks
**Effect: Reduced flow to patient**
| Causes | Controls |
|---|---| 
| Badly attached fitting. | Low tidal volume alarm |
| Debris in airstream | Low tidal volume alarm |
| Damage due to knock or drop. | Low tidal volume alarm |

### Blockage
**Effect: Ventilation failure**
| Causes | Controls |
|---|---| 
| Damage due to knock or drop causing baffles to break loose. | Debris trap, cross-sectional area much greater than pipe cross-sectional area. Low tidal volume alarm. |

### Fails to mix Oxygen and Air.
**Effect: Patient receives inconsistent concentration of oxygen with each breath. Also measured value of Oxygen concentration may not be the real value and clinician may set to too high or low a value. There will still be mixing due to downstream components like flow meter and throttle valve. Screen reader support enabled.**

| Causes | Controls |
|---|---| 
| Baffles damaged or broken due to debris in airstream. | Oxygen sensor not straight after mixing tank, so lots of other devices to cause mixing between mixing tank and Oxygen sensor. Testing before in-service.
Screen reader support enabled. |

## References
FMEA paper from OVSI team
