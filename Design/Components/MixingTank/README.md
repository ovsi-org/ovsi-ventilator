# Mixing tank

Once the Oxygen has been added to the gas path it is necessary to ensure the Air and Oxygen are well mixed so that the Patient receives a gas mixture of a constant Oxygen concentration. This task is performed by the Mixing Tank. The need for the mixing to be performed with minimal pressure drop along with the requirement of a simple, small volume design led to a baffled structure.

The mixing tank is thus situated between straight after the air intake path merges with the O2 supply path, and the exhaust port of the mixing tank leads to the inhale path (see diagram).
![Mixing tank](mixingtank_chart.PNG "Mixing Tank")

The Air-O2 stream is dumped into a cuboid box, the sudden increase in flow cross-sectional area results in recirculation and mixing of the gas stream. Baffles then guide the flow around a series of 180° bends which result in the formation of secondary flows known as Dean vortices, for square ducts these vortices are unsteady down to low Reynolds numbers [2] and result in mixing of the fluid. Separations around the bends also result in recirculation regions that further mix the flow. The pressure loss of the Mixing Tank is circa 150 Pa.

![](CrossSectionMixingTank.png)  
Cross-section of the Mixing tank, showing the baffles that guide the flow around the 180o turns

Anything risk-related with the mixing tank can be found in the [Risks & Failures](Risks&Failures.md) document of this Readme

## Specifications

* **Oxygen compatibility considerations? Assume yes for all?**
* **Medical grade considerations?**
* **Other Specifications?**


## Fabrication/purchasing

**Is this part fabricated or purchased? - Details of fabrication? Is the STEP file all one part becase the original was printed?**

Step of the [mixing tank is in the folder](mixing_tank_v3.stp). ** Where are the original CAD files? - This STEP came from Wittle2, was it updated?**

[2]: Cheng, K. C. (1992). Secondary Flow Phenomena in Curved Pipes and Rotating Channels. In Flow Visualisation VI (pp. 79–89).
