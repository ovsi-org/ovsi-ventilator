# Tidal flow meter

The tidal flow meter (or V-flow meter) measures the total flow rate delivered by the main gas circuit, to provide the clinician with information on the total integrated tidal volume and transient tidal flow to the patient. The requirements for simplicity and low cost drive the design toward a traditional pressure difference device. In addition, accuracy is required at low flow rates of 5 - 80l/min, and low Reynolds Numbers on the order of 100.

The V-flow meter is situated between the second safety relief valve and the Inhale solenoid valve

![V-flow meter](vflow_chart.PNG "V-flowmeter")

A wedge type flow meter is used, due to its ability to provide a relatively constant discharge coefficient at low Reynolds Number, facilitating accurate low flow-rate measurement. The device consists of a circular tube (3) with one-inch male connectors at either end, to connect to the gas circuit. The tube contains a wedge shaped constriction that is oriented perpendicular to the flow (1). As gas flows through the device, a pressure differential is generated, which is measured via static pressure tappings on either side of the constriction (3). The pressure difference is measured by a differential pressure transducer, and converted into a volumetric flow rate by the control electronics. As the conversion algorithm requires knowledge of the gas density, the total pressure and temperature must also be measured at the device inlet or outlet - and the gas composition must also be known - which is known from the measured FiO2. The constriction is sized to provide a differential pressure consistent with the range of a 4inch H2O pressure sensor, when the flow meter is handling the peak flow rate. 

Sensors are plaved in small holes at either end of the wedge (2). 

![](vflowlabelled.PNG)  
Cross section of the flow meter showing the wedge constriction and small holes for sensors.

Risks and Failures associated with the Tidal flow meter can be found in the [Risks & Failures](Risks&Failures.md) document in this folder.

<!--There are two tidal flow meters (V-flow meter) in the device according to the gitlab, but only 1 in the flowchart. One for oxygen and one for the Air/O2 mix. I have supposed that the oxygen flow meter is only needed if the O2 sensors are not in use as suggested by the FMEA; but we'll have to double check and I'll change the readmes if it is the case.-->

## Oxygen Flow Meter

The oxygen flow meter monitors the oxygen flow rate into the gas circuit, so the FiO2 delivered to the patient can be calculated. Normally, the oxygen flow meter handles only a fraction of the total flow in the main gas circuit, but when the ventilator is run at 100% FiO2, the flow meter handles the entire flow. Thus, the peak flow requirements of the oxygen flow meter are the same as for the tidal flow meter downstream, and so the two devices are chosen to be identical.

The operation of the flow meter is the same as described for the Air/O2 Flow Meter below. The only difference in operation is that, regardless of the FiO2 setting, the oxygen flow meter handles a gas composition that is 100% O2. Thus, measurement of the gas composition is not required to calculate the gas density in the flow rate conversion algorithm employed by the electronic system. 

Risks and Failures associated with the Oxygen Flow meter can be found [here](https://gitlab.com/ovsi-org/ovsi-ventilator/-/blob/master/Design/Components/OxygenSensors&Meters/Risks&Failures_flowmeter.md)

## Parts of the design that needs changing 
**rationale for each could be found in the [Risks and Failures document of this folder](Risks&Failures.md)**

- [ ] Could add flow condtioner depending of DP. V flow meter part of assembly that means that upflow geometry is fixed. Don't want to have to calibrate every device.
- [ ] Must take account of effect of Po on flowrate reading
- [ ] Must take account of effect of To on flowrate reading
- [ ] Must take account of effect of oxygen concentration
- [ ] Cd should be calibrated in situ on the production prototype. 
- [ ] The upstream pipe orientation must be fixed for all production versions of the machine. 
- [ ] License built machines must use piping components that will not permit assembly in incorrect orientation. 
- [ ] A batch of flow meters should be tested to ensure manufacturing variations lead to a tolerable scatter of flow rate accuracy
- [ ] Could add flow condtioner depending of DP.
- [ ] Needs to be reevaltuated when electrical system descigned. Need a method of detecting situation in the code.

**Controls must also be found for the following possible failures:**
- [ ] Blocked pressure taps (particulate in gas circuit)
- [ ] Change in oxygen concentration, and subsequently gas constant R and density


## Specifications

* Accuracy flow range: 5 - 80l/min **At what accuracy/precision**
* **Oxygen compatibility considerations?**
* **Medical grade considerations**

## Fabrication/purchasing

**Is this part fabricated or purchased? - Details of fabrication including tolerances to maintain accuracy?**


2 step files of V-Flow meters are provided: [File 1](vflow.stp), [File 2](vflow.stp). **Which one should be used? - Where are the original CAD files?**
