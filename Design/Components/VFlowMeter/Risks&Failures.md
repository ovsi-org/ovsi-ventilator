## Risks and Failures
This document shows the possible risks and failures of the V-flow meter. 

### Error in flowrate reading (too high)
**Effects: Clinician provided with innacurate measurement of total tidal volume. Clinician provided innacurate information of delivery rate to patient**
| Causes | Controls |
|---|---| 
| Sensitivity to upstream pipe orientation | Cd should be calibrated in situ on the production prototype. The upstream pipe orientation must be fixed for all production versions of the machine. License built machines must use upstream components that will not permit assembly in incorrect orientation. |
| Change in altitude Change in Po | Po measurement in vicinity of flow meter |
| Change in altitude Change in To | To measurement in vicinity of flow meter |
| Change in oxygen concentration, and subsequently gas constant R and density | **No current control for this** |

- [ ] Could add flow condtioner depending of DP. V flow meter part of assembly that means that upflow geometry is fixed. Don't want to have to calibrate every device.
- [ ] Must take account of effect of Po on flowrate reading
- [ ] Must take account of effect of To on flowrate reading
- [ ] Must take account of effect of oxygen concentration

### Error in flowrate reading (too low)
**Effects: Clinician provided with innacurate measurement of total tidal volume. Clinician provided innacurate information of delivery rate to patient**
| Causes | Controls |
|---|---| 
| Sensitivity to upstream pipe orientation | include a length of pipe upstream of the flow meter in the injection moulded component to ensure flow conditioning at the measurement location |
| Both pressure tappings disconnected | Sensor will read zero, so problem detectable |
| Change in altitude Change in Po | Po measurement in vicinity of flow meter |
| Change in altitude Change in To | To measurement in vicinity of flow meter |
| Change in oxygen concentration, and subsequently gas constant R and density | **No current control for this** |

- [ ] Cd should be calibrated in situ on the production prototype. 
- [ ] The upstream pipe orientation must be fixed for all production versions of the machine. 
- [ ] License built machines must use piping components that will not permit assembly in incorrect orientation. 
- [ ] A batch of flow meters should be tested to ensure manufacturing variations lead to a tolerable scatter of flow rate accuracy
- [ ] Could add flow condtioner depending of DP.

### Insufficient measurement accuracy at low flow rate
**Effects: Clinician provided with innacurate measurement of total tidal volume. Clinician provided innacurate information of delivery rate to patient**
| Causes | Controls |
|---|---| 
| sensitivity of discharge coefficient at low Reynolds' number | This device has been tuned to measure tidal flowrate |

### Nonsensical flowrate reading
**Effects: Clinician provided with innacurate measurement of total tidal volume. Clinician provided innacurate information of delivery rate to patient**
| Causes | Controls |
|---|---| 
| Blocked pressure taps (particulate in gas circuit) | **No control for this** |
| Leaky/incorrectly fitted soft-tube connections | Low tidal volume alarm |

- [ ] Mark throttle with approximate flow rate settings, to allow user to default to safe position in absence of electronic readout.
- [ ] Robust soft tube connectors to be used on moulded part
- [ ] Start-up check, pressurise the system up to the solenoid valve but keep the solenoid valve closed. 
- [ ] Check the pressure the other side of the solenoid valve is atmostpheric. If we still measure a flow then we know one of the tubes has popped off. Put this software in the non-critical processor.

### Blocks gas circuit
**Effects: Ventilation failure**
| Causes | Controls |
|---|---| 
| Ingress of debris from upstream | Inlet filtering, Low tidal volume alarm, pressure alarm |

- [ ] Needs to be reevaltuated when electrical system descigned. Need a method of detecting situation in the code.

### Depressurisation
**Effects: Ventilation failure**
| Causes | Controls |
|---|---| 
| Fracture due to shock/droppage | Low tidal volume alarm, Low PIP alarm. |

## References
FMEA paper from OVSI team
