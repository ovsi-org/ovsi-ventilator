# Exhale solenoid valve

[[_TOC_]]

## 1. Overall
A solenoid valve is a valve containing a solenoid, which is an electric coil with a movable ferromagnetic core (plunger) in its center.
The exhale solenoid valve is open by defalut, and closed when the solenoid is activated. 

Al possible failures and risks associated with the exhale solenoid valve can be found in the [Risk & Failures](Risks&Failures.md) document of this folder.

Please refer to closely related components:
1. [Inhale solenoid valve](../InhaleSolenoidValve)
2. [Solenoid actuation system](https://gitlab.com/ovsi-org/ovsi-ventilator/-/tree/master/Design/Electronics%20&%20Control/Solenoid%20Actuation%20System)

### 1.1 Location
- The exhale solenoid valve is located between the patient mask and the PEEP valve leading to the exhaust, with some filters in between.

![Exhale Solenoid Valve](exhale_chart.PNG "Exhale Solenoid Valve")

## 2. Design

### 2.1 Features

| Feature | Function | Effect |
|---|---|---|

- The exhale solenoid shares features with the [inhale solenoid valve](../InhaleSolenoidValve#21-features).
    - The exhale solenoid valve also has a linearly actuated plunger valve with spring return arrangement.
    - The valve is also controlled by the solenoid actuation system.
- However, the exhale solenoid valve is of the Normally Open (NO) type so the patient’s safety is guaranteed. See [specification](#specification).[^1]

- [ ] **Any other notable features of the design process? Avoiding duplication with inhale solenoid valve**

### 2.2 Functioning

| State | Action | Reason |
|---|---|---|
| Start of inhalation phase | The actuator pulls the valve shut against the valve seat and the return spring compresses. | |
| Start of exhalation phase | The actuator is de-energised and the spring returns the valve to the open state. | |
 - [ ] **Need to include reasons, perhaps similar to inhale solenoid valve?**

### 2.3 Drawings
![Exhale Solenoid Valve](https://docs.google.com/drawings/d/e/2PACX-1vRrfWpPz4-WHNEo64pI2cF9L5mydrnAbH7Csgj1A9sYF8Sg-u2mGZjwJiO353sGmGTKQ5e7CSus_Ze2/pub?w=960&h=720 "Cross-Section of Exhale Solenoid Valve")

*Cross-section of the exhale solenoid valve.*

- [ ] **Labelling the sub-components of the valve, including the actuator, plunger, valve seat**

## 3. Specification

| Component | Metric | Value | Status | Notes |
|---|---|---|---|---|
| Exhale solenoid valve | Time to switch the valve state | Less than 100 ms | Requirement | For patient comfort |
| Exhale solenoid valve | Solenoid type | Normally open (NO) | Requirement | |

- [ ] **Any other specifications?**

## 4. Parts of current design that nedd changing.
- [ ] Get clinical advice on the failure to open and design of alarm
- [ ] Is it worth condisering a second flow meter in the exhaust system, to compare to the inhale flow meter so a discrepancy between the two would flag up a leak?
- [ ] put pressure sensor just upstream of patient mask

## 5. Maintenance and troubleshooting

Here is a [guide to identify and solve issues with solenoid valves](https://tameson.com/troubleshooting.html)

## 6. Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
[^2]: [Solenoid valve troubleshooting guide](https://tameson.com/troubleshooting.html)
