## Risks and Failures
This document shows the possible risks and failures of the Exhale solenoid valve (normally open type). 
### Fails to open/complete blockage
**Effects: patient can not breathe out**
| Causes | Controls |
|---|---| 
| spring fails | off the shelf component with adequate mechanical properties, low tidal volume alarm on subsequent breaths |
| bushing prevents plunger movement due to debris ingress | inlet and viral filter upstream , HME filter upstream, debris traps,  low tidal volume alarm on subsequent breaths |
| bushing prevents plunger movement due to jamming because of excessive load or wear | valve seat allows radial movement of plunger, so only the busing locates it radially,  low tidal volume alarm on subsequent breaths |
| valve plunger sticks to valve seat due to ingress of contaminants | inlet and viral filter upstream, HME filter breathing out,  low tidal volume alarm on subsequent breaths |
| knock or drop | ventilator designed to withstand 1.5m drop,  low tidal volume alarm on subsequent breaths |

where is the exhalation pressure monitored? For this failure mode, it would be better to monitor the exhalation pressure in between the inhale and exhale solenoid valve, to pick up high exhalation pressure and raise the exhalation pressure above PEEP  alarm. 

- [ ] Get clinical advice on this failure mode and design of alarm
 
### Fails to close
**Effect: wasting pressurised air when inhale solenoid valve opens, patient loses mechanical breathing advantage of ventilator for high lung compliance which will favour air being dumped through the PEEP valve instead of going to the patient**
| Causes | Controls |
|---|---| 
| solenoid actuator burns out | high tidal volume alarm, low PIP alarm because PEEP valve does not allow pressure to rise above PEEP |
| bushing prevents plunger movement due to debris ingress | inlet and viral filter at inlet of system, high tidal volume alarm, , low PIP alarm because PEEP valve does not allow pressure to rise above PEEP |
| debris prevents spring contraction | inlet and viral filter at inlet of system, high tidal volume alarm, ,low PIP alarm because PEEP valve does not allow pressure to rise above PEEP |
| knock or drop | ventilator designed to withstand 1.5m drop, high tidal volume alarm, , low PIP alarm because PEEP valve does not allow pressure to rise above PEEP |

This needs rigorous testing of simulated ventilation failure caused by components upstream and downstream of the patient

### Leaks to outside of main gaspath
**Effect: exhaled air does not pass through second viral filter and intended exhaust, 
wasting pressurised air when inhale solenoid valve opens, patient might breathe out below PEEP.
Fire hazard if high O2 air reaches solenoid (temperature source)**
| Causes | Controls |
|---|---| 
| leaks through plunger shaft bushing | geometric and dimensional tolerancing of plunger shaft to have a small clearance fit between the bushing and shaft. low PEEP alarm, high tidal volume alarm, low PIP alarm |
| leaks because pipe junction comes undone  | solenoid valve not physically connected to compressor, vibration levels are very low,  low PEEP alarm, high tidal volume alarm, low PIP alarm |
| eaks due to damage after knock or drop | ventilator designed to withstand 1.5m drop, ductile material used so leaks before catastrophic fracture,  low PEEP alarm, high tidal volume alarm, low PIP alarm |

If there is a leak in the exhale solenoid valve, the flow meter upstream will not be able to distinguish between air that goes to the patient and air that is leaked. This could mean the tidal volume alarm will not sound even though the volume of air going to the patient drops due to the leak. Is it worth condisering a second flow meter in the exhaust system, to compare to the inhale flow meter so a discrepancy between the two would flag up a leak?

### Leaks between plunger and valve seat
**Effect: geometric and dimensional tolerancing of plunger shaft to have a small clearance fit between the bushing and shaft. low PEEP alarm, high tidal volume alarm, low PIP alarm**
| Causes | Controls |
|---|---| 
| damage to valve seat seal due to debris | inlet and viral filters upstream, HME filter in patient mask, high tidal volume alarm, low  PIP alarm |
| debris preventing full closure of valve | inlet and viral filters upstream, HME filter in patient mask, low PIP alarm, high tidal volume alarm |

If there is a leak in the exhale solenoid valve, the flow meter upstream will not be able to distinguish between air that goes to the patient and air that is leaked. This could mean the tidal volume alarm will not sound even though the volume of air going to the patient drops due to the leak. Is it worth condisering a second flow meter in the exhaust system, to compare to the inhale flow meter so a discrepancy between the two would flag up a leak?

### valve actuation slower than 100ms
**Effect: exhale valve is still open when patient has started inhaling, exhale valve is still closed when patient has started exhaling**
| Causes | Controls |
|---|---| 
| increased friction between plunger shaft and bushing | inlet and viral filters upstream, high tidal volume alarm |
| increased friction between solenoid housing and shaft | inlet filter upstream, debris traps, high tidal volume alarm |
| interference with magnetic field | shielded actuator housing, high tidal volume alarm |

If there is a leak in the exhale solenoid valve, the flow meter upstream will not be able to distinguish between air that goes to the patient and air that is leaked. This could mean the tidal volume alarm will not sound even though the volume of air going to the patient drops due to the leak. 

- [ ] Is it worth condisering a second flow meter in the exhaust system, to compare to the inhale flow meter so a discrepancy between the two would flag up a leak?

### Emits too much EM radiation
**Effect: disrupts other devices, does not meet regulation requirements**
| Causes | Controls |
|---|---| 
| unshielded solenoid actuator | select shielded actuator |

### Complete depressurisation
**Effect: exhaled air does not pass through second viral filter**
| Causes | Controls |
|---|---| 
| knock or drop | ductile materials used to prevent complete fracture of components, ventilator designed for 1.5m drop, low PIP alarm, high tidal volume alarm, low PEEP alarm |

**Effect: inhalation pressure and volume to patient lower than intended as exhale valve is open during inhalation phase**
| Causes | Controls |
|---|---| 
| knock or drop | exhalation pressure below PEEP alarm, high tidal volume alarm, low PIP alarm |

- [ ] put pressure sensor just upstream of patient mask

## References
FMEA paper from OVSI team
