## Risks and Failures
This document shows the possible risks and failures of the Check Valve situated between the air reservoir and the O2 supply. 

### Tube disconnected
**Effects: Ventilation failure, loss of PEEP.**
| Causes | Controls |
|---|---| 
| tube pulled  | low PIP alarm, low PEEP alarm |

### Tube blocked/pinched
**Effects: Ventilation failure, patient unable to breathe in or out**
| Causes | Controls |
|---|---| 
| tube squashed or excessively bent | low tidal volume alarm |

## References
FMEA paper from OVSI team
