# Patient Side Tubing
This component is not part of the OVSI system iteslf, but is related to how the system is connected to the patient.
It is linked to the inhale path, straight after the inhale path check-valve, and the exhale path, right before the exhale path check valve.

The tubes linking the inhale, exhale and patient is called the patient Y-piece.

![Patient side tubing](patientside_chart.PNG "Patient side tubing")

The Patient Side Tubing is an Off-the-Shelf component which needs to be complient to medical requirement.

Risks and failures related to this component can be found in the [Risks & Failures](Risks&Failures.md) document in this folder.

## Specifications
The Patient Y-piece must be medical grade plastic.

## Purchasing
An example of supplier for a patient Y-piece is:
* Supplier: Intersurgical
* part number:1900000 22mm traditional Y-piece 22M/15F patient connection - 22M-22M
* link: https://uk.intersurgical.com/products/critical-care/ypiece-connectors
