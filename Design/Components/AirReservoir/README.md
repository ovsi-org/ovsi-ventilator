# Air reservoir

[[_TOC_]]

## 1. Overall

The air reservoir stabilises unsteadiness in the gas circuit, arising from the dynamics of the [pressure relief valve](../PressureReliefValve) upstream.
**Disclaimer**: The air reservoir is part of the Whittle 2 system but **was flagged as a part to be removed from the system entirely**. See the [Risk & Failures](Risks&Failures.md) document for more information.

In the first design, the Air reservoir was placed between the air intake, after the Pressure relief valve, and the check valve leading to the mixing tank.
![Air reservoir](air_reservoir_chart.PNG)

- [ ] If kept, the Air reservoir should also be adapted to ensure it is correctly fitted, maybe through markings on the parts.

### 1.1 Location

- The air reservoir is located downstream of the [pressure relief valve](../PressureReliefValve) and upstream of the [throttle valve](../ThrottleValve) and [check valve](../CheckValve) downstream in the air intake circuit.

## 2. Design

### 2.1 Features

- A two litre stainless steel tank is used.

## 3. Specification

| Component | Metric | Value | Status | Notes |
|---|---|---|---|---|
| Air reservoir | Internal gauge pressure | 10 kPa | Requirement | To provide a safe margin of operation without rupturing |

- [ ] **What is the tolerance of the 10 kPa value?**

## 5. Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
