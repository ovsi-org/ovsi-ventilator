## Risks and Failures
### Rupture
**Effects: Depressuristaion of system**
| Causes | Controls |
|---|---| 
| Rough Handling | Shock absorbant case; and foam packed around components. Low tidal volume alarm, Low PIP alarm. |
| Knocked over | Shock absorbant case; and foam packed around components. Low tidal volume alarm, Low PIP alarm. |
| Overpressure | Piping connectors and mixing vessel rated to well above compressor pressure rise capability. Low tidal volume alarm, Low PIP alarm. Built to withstand 10kPa |
| Crushed between bed and object | Low tidal volume alarm, Low PIP alarm. |

### Leak
**Effect: Reduced flow to patient**
| Causes | Controls |
|---|---| 
| Incorrect fitting | Standard piping connectors, hard to fit incorrectly. Low tidal volume alarm, Low PIP alarm. |
| Damaged fitting | Standard piping connectors, hard to fit incorrectly. Low tidal volume alarm, Low PIP alarm. |

- [ ] A mark on the vessel and the connector should align when correctly fitted.
- [ ] The design should include documentation and labelling for correct set up.

**Recommendations include removing the Air reservoir entirely**
