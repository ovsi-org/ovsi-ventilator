## Risks and Failures
This document shows the possible risks and failures of the Viral Filter situated between the Exhale Solenloid Valve (NO) and the PEEP Valve. 

### Direct release of exhaled air to atmosphere
**Effects: None - no contamination as air should have been scrubbed by exhale filter #1.  May also drop exhalation pressure below PEEP**
| Causes | Controls |
|---|---| 
| Filter incorrectly fitted | Low PEEP alarm |
| Breakage due to shock/droppage | Low PEEP alarm |

### Fails to filter virus from gas stream
**Effects: None - no contamination as air should have been scrubbed by exhale filter #1**
| Causes | Controls |
|---|---| 
| Filter material breached - rough handling during installation |  |
| Breakage due to shock/drop |  |
| Reaches end of lifespan |  |

### Harbours/allows growth of pathogens (e.g. legionella), which are subsequntly released into gas circuit downstream
**Effects: Contamination of enivronment**
| Causes | Controls |
|---|---| 
| Filter not replaced frequently during long term use, or use between patients |  |

### Pressure drop too high
**Effects: Exhalation pressure too high; patient unable to exhale**
| Causes | Controls |
|---|---| 
| Filter blocked due to particulate matter, water, mould growth | High PEEP alarm |

## References
FMEA paper from OVSI team
