## Risks and Failures
This document shows the possible risks and failures of the Viral Filter situated between the CHeck Valve and the Exhale Solenloid Valve (NO). 

### Direct release of exhaled air to atmosphere 
**Effects: Contamination of environment, depressurisation of system leading to ventilation failure. May also drop exhalation pressure below PEEP, and inhalation pressure will be low**
| Causes | Controls |
|---|---| 
| Filter incorrectly fitted | Low PIP alarm, high tidal volume alaram, low PEEP. If leak small enough, then alarms may not sound but contamination of environment may still occur |
| Breakage due to shock/droppage | Low PIP alarm, high tidal volume alaram, low PEEP. If leak small enough, then alarms may not sound but contamination of environment may still occur |

### Fails to filter virus from gas stream
**Effects: Contamination of exhale solenoid valve. Downstream filter prevents contamination of environment**
| Causes | Controls |
|---|---| 
| Filter material breached - rough handling during installation |  |
| Breakage due to shock/drop |  |
| Reaches end of lifespan |  |

### Harbours/allows growth of pathogens (e.g. legionella), which are subsequntly released into gas circuit
**Effects: Patients exposed to harmful pathogens. Potential contamination of exhale solenoid valve**
| Causes | Controls |
|---|---| 
| Filter not replaced frequently during long term use, or use between patients |  |

### Pressure drop too high
**Effects: Patient unable to exhale**
| Causes | Controls |
|---|---| 
| Filter blocked due to particulate matter, water, mould growth | High PEEP alarm, low tidal volume |

## References
FMEA paper from OVSI team
