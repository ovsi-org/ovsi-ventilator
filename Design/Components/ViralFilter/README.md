# Viral filter

[[_TOC_]]

## 1. Overall

> The viral filter prevents air exhaled by the patient, a likely carrier of viruses, from passing unfiltered through the device and into the environemnt, thus
>   - reducing possibility of infection of those in the vicinity of the device
>   - preventing contamination of the device components, which would render them single-use

### 1.1 Location
- One viral filter is at the air intake.[^1]
    - This filter incorporates a heat and moisture exchanger, and is thus called a heat and moisture exchanger filter (HMEF)
- Two viral filters are on the exhaust line [^3]
- One last viral filter is just prior to the patient. [^3]
Note: There is also a [dust filter](../DustFilter) immediately downstream of the intake viral filter. 


## 2. Design

### 2.1 Features

- The viral filter is an off-the-shelf component.

### 2.2 Functioning

- The viral filter functions as a passive filter through a web of polypropylene fibers with an electrostatic charge that trap contaminants.[^2]
    - When combined with a heat and moisture exchanger as a HMEF, it also captures heat and moisture present in expired breath and returns this to patient upon inspiration.[^2]

### 2.3 Drawings
![Viral Filter - Plusbuyer](20200815_ViralFilterPlusbuyer.jpg "Example of Viral Filter from Plusbuyer")
*Example of a standard 22 mm viral filter. Obtained from Plusbuyer [website](https://www.plusbuyer.com/universal-bacterial-viral-filter-for-cpap-bipap-ventilator-filters-viral-and-airborne-allergens-10pcs-p-10328.html) on 15 August 2020.*

## 3. Specification

| Component | Metric | Value | Status | Notes |
|---|---|---|---|---|
| Viral filter | Inner and outer diameter of connection | 22 mm ID x 15 mm ID/22 mm OD | Requirement | Standard off-the-shelf component, see example from Plusbuyer above |

### 3.1 Requirements
| Requirement | Rationale |
|---|---|
|Size of the pores must not exceed 65nm | This is the necessary size to remove SARS-COV-2 [^4] |

## 4. Manufacture

### 4.1 Fabrication

### 4.2 Purchasing

| Supplier | Part number | Cost per unit (£) | Link | Notes |
|---|---|---|---|---|
| Plusbuyer | A104792861358829798PB | 1.98 | https://www.plusbuyer.com/universal-bacterial-viral-filter-for-cpap-bipap-ventilator-filters-viral-and-airborne-allergens-10pcs-p-10328.html |  |
| Intersurgical | 1690000 | N/A | https://uk.intersurgical.com/products/airway-management/floguard-low-resistance-breathing-filter-for-cpap-and-bilevel | See [PDF specification](20200815_IntersurgicalViralFilter.pdf) for design features and resistance to flow figures |
| AHP Medicals | AHP17099, 9064/711 | 0.64 | https://www.ahpmedicals.com/hmef-electrostatic-filter-with-sampling-port-adult-50-17099.html | Compared to the first two options (viral filters), this is a HMEF (heat and moisture exchanger + viral filter) for use between y-piece and patient |

- Often packaged as 50 pieces
- Check for pressure resistance around expected flow rate range

## 5. Maintenance

- Change within 24 hours

## 6. Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
[^2]: [Filtration and humidification](https://www.intersurgical.com/info/filtrationandhumidification) by Intersurgical, 2020
[^3]: Exploring Possible Sterilisation Methods for the OVSI Ventilator Consultation from Charlie Wedd 01 May 2020.
[^4]: Jeong-Min Kim et al. “Identification of Coronavirus Isolated from a Patient in Korea with COVID-19 Osong Public Health and Research Perspectives”. In: Osong Public Health Res Perspect 11.1 (2020), pp. 3–7. issn: 2233-6052. doi: 10.24171/j.phrp.2020.11.1.02
