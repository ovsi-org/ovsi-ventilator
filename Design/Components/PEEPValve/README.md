# PEEP valve

[[_TOC_]]

## 1. Overall

> The PEEP valve regulates the pressure downstream of the [exhale solenoid valve](../ExhaleSolenoidValve) to the positive end-expiratory pressure (PEEP) - a pressure above that of the ambient atmospheric value. 
>   - This prevents complete depressurisation of the lungs during exhalation which would cause the alveoli to collapse and reduce the tidal volume that can be delivered to the patient.

Please refer to closely related component:
1. [Pressure relief valve](../PressureReliefValve)

- [ ] **Is the PEEP valve identical to the pressure relief valve? If so, why is the specification slightly different, and if not, then CAD drawings and more details are needed.**

### 1.1 Location
- The PEEP valve is located downstream of the exhale solenoid valve and its outlet is to atmosphere.

## 2. Design

### 2.1 Features

- The PEEP valve is a pressure relief valve. **Please refer to the [pressure relief valve](../PressureReliefValve) documentation for common design and features.**

| Feature | Function | Effect |
|---|---|---|


- [ ] **Any other notable features of the design process? Whatever is different to the pressure relief valve documentation.**

### 2.2 Functioning

- A spring loaded poppet blocks the path between the exhale flow and atmosphere, and thus can control the relief pressure.[^1]

### 2.3 Drawings
![PEEP valve]( "Cross-Section of PEEP Valve")
*Cross-section of the exhale solenoid valve.*

- [ ] **Is there a drawing of the PEEP valve?**

## 3. Specification

| Component | Metric | Value | Status | Notes |
|---|---|---|---|---|
| PEEP valve | Supply stable regulation of the exhale system gauge pressure | 5-20 cmH<sub>2</sub>O, adjusted in 5 cmH<sub>2</sub>O increments | Requirement | |

- [ ] **Any other specifications?**

## 4. Manufacture

### 4.1 Fabrication
- Due to an increased demand of off-the-shelf PEEP valves, the valve for this ventilator is designed and manufactured as part of the system.
- [ ] **Need to add fabrication details**

### 4.2 Purchasing

## 5. Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
