## Risks and Failures
This document shows the possible risks and failures of the PEEP Valve located between the Viral Filter and the Exhaust.

### Jammed open
**Effect: PEEP goes to ambient**
| Causes | Controls |
|---|---| 
| Ingress of debris | Valve guard, inlet filter, low PEEP alarm |
| Breakage due to shock/drop | Shock absorbant case, foam around components, low PEEP alarm |
| Spring gets bent due to forcing | low PEEP alarm |
| Spring fatigue and breakage | low PEEP alarm |

### Depressurisation
**Effect: PEEP goes to ambient**
| Causes | Controls |
|---|---| 
| Breakage due to shock or drop | Low PEEP alarm |

### Leaks
**Effect: Reduced PEEP if leak is large enough for valve to be unable to compensate**
| Causes | Controls |
|---|---| 
| Breakage due to shock or drop | Low PEEP alarm |

### Setting self-adjusting
**Effect: Incorrect setting of PEEP pressure.If PEEP is less than the pressure in the lungs but higher than the optimal value patient does not exhale fully and does not recieve enough air in each breath. If the pressure falls to far below desired PEEP the alveoli will collapse**
| Causes | Controls |
|---|---| 
| Vibration | PEEP out of range alarm, exhalation pressure monitored. Handle sprung to prevent self-adjusting |
| Spring coroded or snaps | PEEP out of range alarm, exhalation pressure monitored. Handle sprung to prevent self-adjusting |

### Thread jams - Half open
**Effect: No adjustment of PEEP pressure**
| Causes | Controls |
|---|---| 
| Breakage due to shock/drop | Thread to has lead in and large enough to be robust, cross threading therefore unlikely. If jammed to wrong PEEP a PEEP out of range alarm will sound |
| Ingres of debris | Inlet filter, debris traps.  If jammed to wrong PEEP a PEEP out of range alarm will sound |

### Jammed closed
**Effect: PEEP pressure set at the inhalation pressure and patient is unable to exhale**
| Causes | Controls |
|---|---| 
| Ingress of debris | Valve guard; High PEEP alarm, low tidal volume alarm on sunsequent breaths |
| Breakage due to shock/drop | Hard case. High PEEP alarm, low tidal volume alarm on sunsequent breaths |

### Excessive oscillation
**Effect: PEEP fluctuations**
| Causes | Controls |
|---|---| 
| Oscillating mode of valve due to large lift |  |

### Fatigue failure of mechanism due to vibration 
**Effect: Loss of PEEP**
| Causes | Controls |
|---|---| 
| Localised aerodynamic instability causing vibration of poppet | Problem mitigated through aerodynamic design of the poppet including grooving to mitigate vortex sheddding. PEEP alarm |

## References
FMEA paper from OVSI team
