## Risks and Failures
This document shows the possible risks and failures of the second Safety relief valve. 

### Leak
**Effects: Depressurisation - throttle valve and pressure relief valve can compensate, then if move severe there would be insufficient flow to patient. Potential ventilation failure
Screen reader support enabled.**
| Causes | Controls |
|---|---| 
| Big leak (mechanism/connectors) | Low tidal volume alarm will sound, low pressure alarm will sound |
| Fracture of pressure housing due to shock/drop | Low tidal volume alarm will sound, low pressure alarm will sound, valve guard |
| Ingress of debris, blocking piston | Low tidal volume alarm will sound, low pressure alarm will sound |
| Device inverted (e.g. device hung sideways on bedside, and slightly overhung) | Low tidal volume alarm will sound, low pressure alarm will sound |
| Breakage of piston housing due to shock/drop, leading to piston jam | Low tidal volume alarm will sound, low pressure alarm will sound |

- [ ] Inclinometer? Clear labelling must sit flat. Future design to allow hung on side of bed. Increase angle of cone to facilitate large angles of incline.

- [ ] Training must include explanation of correct device orientation

### Triggers at pressure that is too low (within desired inhlaation pressure range)
**Effects: Desired inhalation pressure cannot be reached**
| Causes | Controls |
|---|---| 
| Device incorrectly oriented (not horizontal) | Low tidal volume alarm alarm will sound, low pressure alarm will sound |

- [ ] Many failures are caused by device not being horizontal. It would be good to have a safety mechanism ensuring that it stays horizontal. May include an inclinometer? Clear labelling must sit flat. Future design to allow hung on side of bed. Increase angle of cone to facilitate large angles of incline.

### Permits no/inadequate pressure relief
**Effects: No failure for single failure. Overpressurisation would not occur if  pressure switch and regulator are operating correctly. But failure would remove remove redudency of safety relief system.**
| Causes | Controls |
|---|---| 
| Ingress of debris, blocking piston | SPressure switch & regulator, valve guard |
| Breakage of piston housing due to shock/drop, leading to piston jam | Pressure switch & regulator |
| Ventilator run with box open and sheet placed over exit port | Pressure switch & regulator |

- [ ] Low severity rating due to single failure, but discussion has identified high potential impact in event of two point failures (pressure relief valve 1 or pressure switch).

### Introduces mechanical debris to gas path
**Effects: Blockage of components downstream; obstruction of gas path. Ventilation failure**
| Causes | Controls |
|---|---| 
| Shock/Droppage | Low tidal volume alarm will sound |

- [ ] Make a debris trap

### Blocks flowpath
**Effects: No flow to patient - ventilation failure**
| Causes | Controls |
|---|---| 
| Ingress of debris from upstream | Low tidal volume alarm will sound,  low pressure alarm, Filtering upstream |
| Breakage of piston housing due to shock/drop, leading to piston dropping into gas cricuit | Low tidal volume alarm will sound,  low pressure alarm, valve on branch to main circuit |

- [ ] Need to double check that piston dimensions are not sufficiently small that it is allowed to leave the valve branch and block main circuit.

## References
FMEA paper from OVSI team
