## Risks and Failures
This document shows the possible risks and failures of the first Safety relief valve. 

### Leak
**Effects: Depressurisation of system - throttle valve can compensate to a certain extent, s then if move severe there would be insufficient flow to patient**
| Causes | Controls |
|---|---| 
| Big leak (mechanism/connectors) | Low tidal volume alarm will sound, low pressure alarm will sound |
| Fracture of pressure housing due to shock/drop | Low tidal volume alarm will sound, low pressure alarm will sound |
| Ingress of debris via exit port, blocking piston | Low tidal volume alarm will sound, low pressure alarm will sound,  valve guard |
| Device inocrrectly oriented (not horizontal) | Low tidal volume alarm will sound, low pressure alarm will sound |
| Breakage of piston housing due to shock/drop, leading to piston jam | Low tidal volume alarm will sound, low pressure alarm will sound |

### Triggers at pressure that is too low (within desired inhlaation pressure range)
**Effects: Desired inhalation pressure cannot be reached**
| Causes | Controls |
|---|---| 
| Device incorrectly oriented (not horizontal) | Low tidal volume alarm alarm will sound, low pressure alarm will sound |

- [ ] Many failures are caused by device not being horizontal. It would be good to have a safety mechanism ensuring that it stays horizontal. May include an inclinometer? Clear labelling must sit flat. Future design to allow hung on side of bed. Increase angle of cone to facilitate large angles of incline.

- [ ] Also consider including a Rubber shock absorber (O ring) to prevent potential cracks.

### Permits no/inadequate pressure relief
**Effects: No failure effect for this as a single point of failure. Overpressurisation but Safety Relief Valve #2 activated. But failure would remove remove redudency of safety relief system**
| Causes | Controls |
|---|---| 
| Ingress of debris via exit port, blocking piston | Safety relief valve #2, valve guard |
| Breakage of piston housing due to shock/drop, leading to piston jam | Safety relief valve #2 |
| Ventilator run with box open and sheet placed over exit port | Safety relief valve #2 |

### Introduces mechanical debris to gas path
**Effects: Blockage of downstream components, obstruction of gas path. Ventilation failure**
| Causes | Controls |
|---|---| 
| Shock/Droppage | Component made of ductile material, debris traps in flow path |

- [ ] Make component out of ductile material, height changes currently act as debris traps but it might be worth considering introducing formal debris traps in pipes.

### Blocks flowpath
**Effects: No flow to patient - ventilation failure**
| Causes | Controls |
|---|---| 
| Ingress of debris from upstream | Low tidal volume alarm will sound,  low pressure alarm, Filtering upstream, valve on branch to main circuit |
| Breakage of piston housing due to shock/drop, leading to piston dropping into gas cricuit | Low tidal volume alarm will sound, valve on branch to main circuit |

- [ ] Need to double check that piston dimensions are not sufficiently small that it is allowed to leave the valve branch and block main circuit.

## References
FMEA paper from OVSI team
