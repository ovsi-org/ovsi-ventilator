# Safety relief valve

[[_TOC_]]

## Overall

Two safety relief valves are used to prevent over-pressurisation of the gas circuit past 80 cmH<sub>2</sub>O, which could injure the patient.
One safety relief valve is situated between the compressor and the Air reservoir (1). The other one is between the mixing tank and the V-flow meter (2).
![Safety relief valves in the flowchart](safetychart.PNG "Safety relief valves 1 and 2 in the flowchart")

Anything related to the possible the risks and failures of the Safety Relief valves can be found in the Risks and Failure documents for the [First](Risks&Failures_1.md) and [Second](Risks&Failures_2.md) valves.

| Safety relief valve | Location | Function |
|---|---|---|
| Safety relief valve 1 | Adjacent to the [pressure relief valve](../PressureReliefValve) | To mitigate overpressurization by the [compressor](../Compressor), in the event of the pressure relief valve malfunctioning |
| Safety relief valve 2 | Outlet of [mixing tank](../MixingTank) | To mitigate malfunction and overpressurisation by the variable oxygen supply system |

*Note: the operating requirements for both valves are the same.*

Safety Relief valves have 3 components:
* A [valve body](safety_body.stl) which consists of a chamber, a housing for the piston and holes along the housing for the ir to escape when the pressure drives the piston upwards
* A [lid](safety_lid.stl) which retains the piston from escaping, and the holes allow for the housing to remain at athmospheric pressure
* A stainless steel piston, which slides up and down the body housing and closes the chamber when the pressure is low enough.

### Design

- A weighted piston design is used, for its simplicity and reliability. 
    - A stainless steel piston is located in a housing.
    - This piston housing allows freedom of movement in the vertical direction. 
    - The housing is open to atmospheric pressure through perforations in the walls and lid. 

### Functioning

| State | Gas circuit pressure (cm H<sub>2</sub>O) | Action |
|---|---|---|
| Normal operation | Less than 80 | The piston is held down by its own weight, against a valve seat containing an orifice leading out from the main gas circuit. This seals the gas circuit.  |
| Overpressurisation | More than 80 | The piston is forced upward in the housing, allowing fluid from the gas circuit to flow out of the orifice, and leave the valve through the perforations in the housing walls. This relieves the pressure in the system when the gas circuit pressure rises above 80cmH<sub>2</sub>O. |

### Drawings
![Safety relief valve](safetyrelief_labelled.PNG "Cross-Section of Safety Relief Valve")
*Cross-section of a safety relief valve showing the stainless steel piston in the closed position.*

## Parts of the design that need changing
Rationale for these can be found in the risks and failures documents for the [First](Risks&Failures_1.md) and [Second](Risks&Failures_2.md) valves.
- [ ] Many failures are caused by device not being horizontal. It would be good to have a safety mechanism ensuring that it stays horizontal. May include an inclinometer? Clear labelling must sit flat. Future design to allow hung on side of bed. Increase angle of cone to facilitate large angles of incline.
- [ ] Inclinometer? Clear labelling must sit flat. Future design to allow hung on side of bed. Increase angle of cone to facilitate large angles of incline.
- [ ] Training must include explanation of correct device orientation
- [ ] Also consider including a Rubber shock absorber (O ring) to prevent potential cracks.
- [ ] Low severity rating due to single failure, but discussion has identified high potential impact in event of two point failures (pressure relief valve 1 or pressure switch).
- [ ] Need to double check that piston dimensions are not sufficiently small that it is allowed to leave the valve branch and block main circuit.
- [ ] Make component out of ductile material, height changes currently act as debris traps but it might be worth considering introducing formal debris traps in pipes.
- [ ] Make a debris trap


## Specification

- [ ] **Any further specification details beyond the 80 cmH<sub>2</sub>O threshold?**

## Manufacture

### 1. Fabrication

The parts for this can be manufactured *are there any files for the piston?*

### 2. Purchasing

Medical grade safety relief valves can also be purchased 
link https://www.wittgas.com/products/gas-safety-equipment/safety-relief-valves/safety-relief-valve-av-319/ 
*price?*


## Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
