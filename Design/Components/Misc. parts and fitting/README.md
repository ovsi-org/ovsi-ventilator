# Misc.Parts and fitting
Contains all of the bits that don't explicitely belong to other components or whose place it not obvious from the looks of it and the white paper.

## bits that will likely stay there 
electronic spacers 1 and 2 as well as screen tray.

## bits for which I have a theory:
Both the fitting covers may belong to the solenoid valves but it's hard to tell from the diagrams in the white paper. To be double checked with Ruhi and/or Ben.
Outlet and outlet covers could be to any valve. The white paper mentions an outlet for each one.

## weird ones
The Volute_VC_4 file is one that I couldn't find where it is meant to be. Same with ca_d_1 whose name is not explicit at all and I couldn't place it anywhere.
