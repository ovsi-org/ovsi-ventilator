# Inhale solenoid valve

[[_TOC_]]

## 1. Overall

> The inhale solenoid valve has two functions:
>   - Allows flow to the patient during the inhalation phase **(at a specified tidal volume and pressure - check)**
>   - Isolates the patient from the pressurised air to allow the patient to exhale.

Please refer to closely related components:
1. [Exhale solenoid valve](../ExhaleSolenoidValve)
2. Solenoid actuation system

- [ ] **Add links later to solenoid actuation system under electrical system documentation**

### 1.1 Location
- The inhale solenoid valve is located at the outlet of the [mixing tank](../MixingTank).

## 2. Design

### 2.1 Features

| Feature | Function | Effect |
|---|---|---|
| The inhalation solenoid valve allows air flow control. | This enables the ability to cut off the air supply in the exhalation phase. | This drastically reduces the oxygen consumption of this ventilator compared to a standard CPAP device. |

- The inhale solenoid valve consists of an electric linear actuator.
- The valve is controlled by the solenoid actuation system.
- The valve is of the normally closed (NC) type, see [specification](#specification).[^1]

- [ ] **Any other notable features of the design process?**

### 2.2 Functioning

| State | Action | Reason |
|---|---|---|
| Start of inhalation phase | The actuator pulls a plunger away from a valve seat. | This allows air of a specified tidal volume at specified inhalation pressure to flow to the patient through the inhalation valve. |
| Start of exhalation phase | The actuator is de-energised and a spring, which is compressed during the inhalation phase, pushes the plunger back to position on the valve seat. | This stops the flow of air to the patient to allow them to breathe out at the specified exhalation pressure. |

### 2.3 Drawings
![Inhale Solenoid Valve](https://docs.google.com/drawings/d/e/2PACX-1vRhnLeTEUDSSTuDvqIQAl3_S8BYR0YU-pJjnwmPrEqzdkGarqe_BCM6EFEGISKssU-fiiov2sGDtop6/pub?w=960&h=720 "Cross-Section of Inhale Solenoid Valve")
*Cross-section of the inhale solenoid valve.*

- [ ] **Labelling the sub-components of the valve, including the actuator, plunger, valve seat**

## 3. Specification

| Component | Metric | Value | Status | Notes |
|---|---|---|---|---|
| Inhale solenoid valve | Time to switch the valve state | Less than 100 ms | Requirement | For patient comfort |
| Inhale solenoid valve | Solenoid type | Normally closed (NC) | Requirement | |

- [ ] **Any other specifications?**

## 4. Manufacture

- [ ] **Need to add manufacturing details**

### 4.1 Fabrication

### 4.2 Purchasing

## 5. Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
