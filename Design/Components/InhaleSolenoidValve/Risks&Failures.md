## Risks and Failures
This document shows the possible risks and failures of the Inhale Solenoid Valve (normally closed type) located between the O2 sensor and Emergency Inhale Check Vent.

### Fails to open
**Effect: patient is no longer ventilated with pressurised air, patient breathes atmospheric air through emergency inhale check valve**
| Causes | Controls |
|---|---| 
| burned out solenoid actuator | low tidal volume alarm, low inhalation pressure alarm |
| plunger is stuck to valve seat because of ingress of contaminants | inlet and viral filter upstream, low tidal volume alarm, low inhalation pressure alarm |
| debris in flow path preventing plunger movement | inlet and viral filter upstream, low tidal volume alarm, low inhalation pressure alarm, debris traps upstream |
| excessive bearing wear jams plunger shaft | allow plunger to align itself in assembly, low inhalation pressure alarm |

### Fails to close
**Effect: patient has constant supply of pressurised air, venting of air through the exhale solenoid valve when it opens. Exhale pressure will be higher than PEEP if PEEP valve is too small to deal withfull ventilator flow**
| Causes | Controls |
|---|---| 
| spring fails | of the shelf component with adequate mechanical properties, high tidal volume alarm, probably high PEEP alarm |
| jams in bushing due to excessive load or wear | valve seat allows radial movement of plunger, so only the busing locates it radially, high tidal volume alarm, probably high PEEP alarm |
| debris preventing plunger closing movement or spring movement | inlet and viral filter upstream, high tidal volume alarm, probably high PEEP alarm |
 

### Leaks to outside main gaspath
**Effect: Patient receives lower tidal volume than intended. Because flow meter is upstream, the low tidal volume alarm is not raised. This is a problem if the leakage flow is larger than the allowed tolerance band in flow measurement. 

There is a fire hazard if O2 leaks straight to solenoid (temperature source)**
| Causes | Controls |
|---|---| 
| leaks through plunger shaft bushing | geometric and dimensional tolerancing of plunger shaft to have a small clearance fit between the bushing and shaft. Leakage unlikely to be larger than allowed tolerance band in mass flow measurement error, low PIP alarm |
| leak through pipe junction | shock absorbant material to dissiapte energy of solenoid valve movement shocks, low  PIP alarm |
| fractured valve housing due to knock or drop | ventilator designed to withstand 1.5m drop, low tidal volume alarm,choose O2 compatible solenoid, low PIP alarm |

### Leaks between plunger and valve seat (i.e. does not close fully)
**Effect: wasting pressurised air and potentially O2 through the exhale solenoid valve**
| Causes | Controls |
|---|---| 
| damage to valve seat seal due to debris | inlet and viral filters upstream, high tidal volume alarm, debris traps |
| debris preventing full closure of valve | inlet and viral filters upstream, high tidal volume alarm, debris traps |

### Valve actuation slower than 100ms
**Effect: inhale valve is still open when patient has started exhaling, inhale valve is still closed when patient has started inhaling. if the patient starts inhaling and the solenoid opens too slwoly, the pressure to the patient could drop below minimum and collapse alveoli**
| Causes | Controls |
|---|---| 
| increased friction between plunger shaft and bushing | inlet and viral filters upstream, low tidal volume alarm, low inhalation pressure alarm by transducer on patient side of inhalation solenoid valve |
| increased friction between solenoid housing and shaft due to ingress of debris | inlet filter upstream, debris traps, low tidal volume alarm, low inhalation pressure alarm |
| interference with magnetic field | shielded actuator housing, low tidal volume alarm, low inhalation pressure alarm |

### Emits too much EM radiation
**Effect: disrupts other devices, does not meet regulation requirements**
| Causes | Controls |
|---|---| 
| unshielded solenoid actuator | select shielded actuator |

### Complete depressuriation 
**Effect: ventilation failure, risk of introducing debris in the flow path**
| Causes | Controls |
|---|---| 
| knock or drop | low PIP alarm, low tidal volume alarm, ductile materials used to prevent small fragments of debris, HME filter in patient mask |

## References
FMEA paper from OVSI team
