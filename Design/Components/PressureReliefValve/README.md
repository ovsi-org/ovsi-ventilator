# Pressure relief valve

[[_TOC_]]

## 1. Overall

### 1.1 Description

> The pressure relief valve is used to manually control the patient inhalation pressure.

*Please refer to the [PEEP valve](../PEEPvalve) as a type of pressure relief valve.*

- [ ] **If possible, include links to glossary of key definitions of terms like patient inhalation pressure**
- [ ] **Include list of sub-components and their function**

### 1.2 Location

- The pressure relief valve is located downstream of the [compressor assembly](../Compressor), after which follows a [throttle](../ThrottleValve) and [check valve](../CheckValve) before the air flow is mixed with the air-O<sub>2</sub> flow.

## 2. Design

### 2.1 Features

- A spring-loaded poppet valve is used. 

| Objective | Solution |
|---|---|
| Precise adjustment of spring | Threads on handle mounting are finely pitched |
| Maximise pressure relief range | Tapering spring to provide ramp in spring rate with compression force |
| Avoiding vibration of valve head (a common problem of poppet valve design which can limit component lifespan) | Aerodynamic design |
| Avoiding large scale pressure fluctuations, due to the poppet oscillating between the fully open and closed position | Careful matching of the spring to the required relief flow rate and outlet orifice size |

### 2.2 Functioning
- The relief pressure is controlled via compression of the spring, which is adjusted using a screw-mounted handle.

### 2.3 Drawings
![Pressure Relief Valve](PressureReliefValve.png "Cross-Section of Pressure Relief Valve")
*Cross-section of the pressure relief valve, showing the spring loaded poppet and threaded handle to adjust spring compression.[^1]*

- [ ] **Label the 3D CAD image with sub-components**

## 3. Specification

| Component | Metric | Value | Status | Notes |
|---|---|---|---|---|
| PEEP valve | Supply stable regulation of the gas circuit gauge pressure | 15-50 cmH<sub>2</sub>O, adjusted in 2 cmH<sub>2</sub>O increments | Requirement | |

- [ ] **Include other key specification requirements of sub-components and medical standards**

## 4. Manufacture

### 4.1 Fabrication

- [ ] **Include originial CAD files in STEP format and procedure of fabrication**

### 4.2 Purchasing

- [ ] **If some components can be purchased, please list Supplier, Part No., Link and any other key pieces of information**

## 5. Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
