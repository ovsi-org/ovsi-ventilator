## Risks and Failures
This document shows the possible risks and failures of the Pressure Relief Valve located between the Safety Relief Valve #1 and the Air Reservoir.

### Jammed open
**Effect: Depressurisation**
| Causes | Controls |
|---|---| 
| Ingress of debris | Valve guard, inlet filter |
| Breakage due to shock/drop | Shock absorbant case, foam around components. |
| Spring gets bent due to forcing | None |
| Spring fatigue and breakage | None |

### Setting self-adjusting
**Effect: Incorrect setting patient**
| Causes | Controls |
|---|---| 
| Vibration | Alarms, pressure monitored |
| Spring coroded or snaps | Valve upstream of oxygen inlet |

### Thread jams - Half open
**Effect: No adjustment of system pressure**
| Causes | Controls |
|---|---| 
| Misuse;
Hit | Thread to has lead in and large enough to be robust, cross threading therefore unlikely |

### Jammed closed
**Effect: Overpressurisation, but safety relief valves prevent pressure going above 80cmH2O**
| Causes | Controls |
|---|---| 
| Ingress of debris | Valve guard; safety relief valve prevents damage to patient |
| Breakage due to shock/drop | None |

### Excessive oscillation
**Effect: Pressure fluctuations in system**
| Causes | Controls |
|---|---| 
| Oscillating mode of valve due to large lift | Smart design with continuous bleed to stabilise lift |

## References
FMEA paper from OVSI team
