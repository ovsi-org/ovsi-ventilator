## Risks and Failures
This document shows the possible risks and failures of the power supply system of OVSI.

### Loss of power and problems in power supply
**Effects: Ventialtor switches to battery power. Ventilation failure will occur once battery expires**
| Causes | Controls |
|---|---| 
| Power cut | The power management system contains a power mux with automatic switchover, which will switch immediately to the backup battery. Capcitors store the charge in the system to make the switching seamless. The alarm system will inform the clinician that the system has switched to the battery. |
| Voltage surge | A surge protector is embedded in the medically approved AC/DC converter used in the design and additionaly bidirectional/unidirectional high power diodes are used to ensure the safety of the circuit
| Undervoltage | In case of undervoltage, the converter will try to regulate the voltage and produce the desired DC output, however in case that is not possible, the power mux will switch to the backup battery 
| Overvoltage | The AC/DC converter contains overvolatge protection which meets the medical regulations. Moreover, additional  protection circuitry is connected at the DC output of AC/DC converter which ensures that the voltage does not surpass 14V. 
| Overcurrent | The AC/DC converter contains overvolatge protection which meets the medical regulations. Since the current output of the AC/DC converter could reach up to 6A, fuses are used at the output to protect the rest of the system from overcurrent. 
| Voltage ripple/fluctuations | Voltage ripples are handled within the AC/DC converter and additionally capacitors are added to make sure no ripples are introduced to DC/DC converters since that could be detrimental to them
| Unstable power supply | In case of unstable power supply, the system will switch to the battery automatically, while when the system provides sufficient voltage, the battery will be charged
| Not providing enough power | In this case some of the components will have to be replaced and a redesign will be required. However, the design of the system allows components to be changed with more suitable ones without changing the full design of the system

## References
FMEA paper from OVSI team
