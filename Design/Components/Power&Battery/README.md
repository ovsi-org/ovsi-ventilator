# Power Supply and Battery Management

all informations about the risks and failures of this system can be found [here](Risks&Failures_power.md) for the power management system and  [here](Risks&Failures_battery.md) for the battery management system.
## Specifications 
## Regulations
Alongside the main medical device standard (BS EN 60601-1:2006+A12:2014), additional requirements specifically for ventilators are described within BS EN ISO 80601‑2‑12:2020 (as of 2nd August 2021, still available to view online for free https://www.iso.org/obp/ui#iso:std:iso:80601:-2-12:ed-2:v1:en). 

Particular standards relating to power supply and battery management include:
**201.11.8.101 Additional requirements for interruption of the power
supply/supply mains to ME equipment** - The ventilator must include an internal power source which is capable of powering the ventilator for at least 30 minutes in the event of mains power supply failure. There also needs to be a means to measure the remaining capacity or operating time provided by the internal power source. The system must be able to determine which power source is currently powering the device at any given time. An alarm system must also be capable of detecting power switchover and alert the user of remaining operating time. Vigorous testing is also required to characterise ventilator behaviour during the switching states to ensure operation remains safe.



## Purchasing
## References
