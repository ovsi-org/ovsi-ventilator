## Risks and Failures
This document shows the possible risks and failures of the battery pack (emergency power supply). This FMEA is done assuming the power supply has failed already, because the battery will not be required without a power supply failure first.

### low charge when power supply failure occurs
**Effects: premature ventilation failure, less than expected time available without supplying power**
| Causes | Controls |
|---|---| 
| Clinician fails to respond quickly to power supply failure alarm | when battery eventually runs out, it is clear the ventilator is switched off. |
| Contacts coroded due to being uncharged for long period. | when battery eventually runs out, it is clear the ventilator is switched off. |
| Old battery unit, which fails to hold charge | when battery eventually runs out, it is clear the ventilator is switched off. |
| Battery not charged during normal use | when battery eventually runs out, it is clear the ventilator is switched off. |

### No charge when power supply failure occurs
**Effects: Ventilation failure**
| Causes | Controls |
|---|---| 
| Contacts coroded due to being uncharged for long period. | ventilator clearly switched off |
| Old battery unit, which fails to hold charge | ventilator clearly switched off |
| Battery not charged during normal use | ventilator clearly switched off |

### Overheating leading to fire hazard
**Effects: Ventilation failure, exposure of ward to smoke/fire**
| Causes | Controls |
|---|---| 
| Mishandling leading to internal battery shorting/failure | visible smoke |
| Short circuit | visible smoke |
| Charging problem | visible smoke |

- [ ] Documentation of proper care for battery could be included.

## References
FMEA paper from OVSI team
