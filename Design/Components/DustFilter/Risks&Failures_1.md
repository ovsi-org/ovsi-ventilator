## Risks and Failures
This document shows the possible risks and failures of the Inlet Filter (two filters - dust and removes>2mm) located at the air intake of the ventilator. 

### Blocked
**Effect: Stall compressor, ventilation failure;
no motor cooling leading to overheat and potential fire/smoke**
| Causes | Controls |
|---|---| 
| Dusty/sandy environment. Inlet covered by sheet/towel etc. Ingestion of water or other liquid | Intake angled down at 5deg so water can't feed in through gravity. Low tidal volume alarm, low PIP alarm, compressor is temperature monitored |

### Partially blocked
**Effect: Reduced flow to patient leading to possible ventilation failure; motor cooling reduced leading to possible fire/smoke.
higher compressor DP dealt with by pressure relief valve**
| Causes | Controls |
|---|---| 
| Dusty/sandy environment. Inlet covered by sheet/towel etc.  Ingestion of water or other liquid | Intake angled down at 5deg so water can't feed in through gravity. Low tidal volume alarm, low PIP alarm, compressor is temperature monitored |

### Breached
**Effect: Accelerated wear of compressor due to particle ingress. Motor coooling bypass liable to blockage. Inlet debris will be stopped by downstream viral filter**
| Causes | Controls |
|---|---| 
| Item poked into inlet. Reaches end of lifetime. Knock or drop |  |

### Release debris
**Effect: If filter itself is ingested , it could tangle and block compressor leading to motor overheat and smoke/fire and ventilation failure. Accelerated compressor wear. Possible blockage of motor cooling bypass leading to motor overheat and smoke/fire**
| Causes | Controls |
|---|---| 
| Item stuck into inlet. Reaches end of lifetime. Knock or drop | Downstream viral filter. Low tidal volume alarm, compressor is temperature monitored |

### Leakage of filter casing
**Effect: Inlet air is from box rather than atmosphere, but the only way for air to enter the box is through the dust filter. There is no failure effect as a result**
| Causes | Controls |
|---|---| 
| Shock or drop mishandling | Downstream viral filter protects patient and rest of system |

## References
FMEA paper from OVSI team
