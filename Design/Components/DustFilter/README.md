# Dust filter

[[_TOC_]]

## 1. Overall

> The dust filter removes dust and macroparticules from the air flow.

### 1.1 Location

- One dust filter is placed at the air intake, inside the casing.

## 2. Design

### 2.1 Features

- The dust filter is an off-the-shelf component.

### 2.2 Functioning

- The dust filter functions as a passive filter

- [ ] **How does it work?**

### 2.3 Drawings
![Inhale Solenoid Valve](https://docs.google.com/drawings/d/e/2PACX-1vRhnLeTEUDSSTuDvqIQAl3_S8BYR0YU-pJjnwmPrEqzdkGarqe_BCM6EFEGISKssU-fiiov2sGDtop6/pub?w=960&h=720 "Cross-Section of Inhale Solenoid Valve")
*Cross-section of a standard dust filter.*

- [ ] **Find a drawing of a standard dust fitler**

## 3. Specification

| Component | Metric | Value | Status | Notes |
|---|---|---|---|---|
| Dust filter | Cross-section | 100 mm x 50 mm | Requirement | Standard off-the-shelf component |
| Dust filter | Pressure drop | 100 Pa at 160 l / min | Measurement | - [ ] **How was this pressure drop determined?** |

## 4. Manufacture

### 4.1 Fabrication

### 4.2 Purchasing

- [ ] **Need example of dust filter and link to purchase, if possible**

## 5. Maintenance

- [ ] **How often and what is the method of changing the dust filter?**

## 6. Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
