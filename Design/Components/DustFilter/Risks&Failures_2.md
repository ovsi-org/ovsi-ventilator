## Risks and Failures 
This document shows the possible risks and failures of the Box Exhaust Port (and dust filter) located after the PEEP Valve.

### Has too high of a pressure drop
**Effect: Box will pressurise slightly due to exhaust from the motor cooling bypass and the pressure relief valve. General box leaks should stop this getting too dangerous but it will increase the required internal gas pressure required to trigger the safety relief valve. Would reduce motor coolant flow which could lead to overheating and fire/smoke. Gas pressure will rise**
| Causes | Controls |
|---|---|
| Dirty or wet filter | Inlet dust filter. Motor temepratures monitored. Absolute pressure transducer. There's a second box exhaust. High PIP alarm | 
| Debris from inside blown in front of port | Inlet dust filter.  Motor temepratures monitored. Absolute pressure transducer. There's a second box exhaust. High PIP alarm | 
| Item placed in front externally | Absolute pressure transducer so that increases system pressure can be measured. There's a second box exhaust. High PIP alarm | 

### Completely blocks
**Effect: Box will pressurise due to exhaust from the motor cooling bypass and the pressure relief valve. General box leaks should stop this getting too dangerous but it will increase the required internal gas pressure required to trigger the safety releif valve. Would reduce motor coolant flow which could lead to overheating. Motor cooling flow gows through main gas path. Gas pressure will rise**
| Causes | Controls |
|---|---| 
| Dirty or wet filter | Inlet dust filter.  Motor temepratures monitored. Absolute pressure transducer. There's a second box exhaust. High PIP alarm | 
| Debris from inside blown in front of port | Inlet dust filter.  Motor temepratures monitored. Absolute pressure transducer. There's a second box exhaust. High PIP alarm | 
| Item placed in front externally | Absolute pressure transducer so that increases system pressure can be measured. There's a second box exhaust. High PIP alarm | 

### Filter dislodged
**Effect: Potential gap in box through which debris or foreign objects (sheets, biological hazards etc.) could enter. Flow will always be outwards so this helps to reduce the likelyhood**
| Causes | Controls |
|---|---| 
| Knock or drop |  | 

## References
FMEA paper from OVSI team
