## Risks and Failures
This document shows the possible risks and failures of the automotive lambda type of O2 sensor. 

### Error: Fi02 reading erroneously high
**Effects: Clinician recieves innacurate feedback, consequently feeds the patient an Fi02 that is lower than intended**
| Causes | Controls |
|---|---| 
| Calibration error due to temperature and pressure changes. | Measurement of temperautre and pressure in gas circuit provide means to correct calibration in software |
| Sampled gas not fully mixed | Mixing tank upstream of sensor |

### Error: Fi02 reading erroneously low
**Effects: Clinician recieves innacurate feedback, consequently feeds the patient an Fi02 that is higher than intended**
| Causes | Controls |
|---|---| 
| Calibration error due to temperature and pressure changes.  | Measurement of temperautre and pressure in gas circuit provide means to correct calibration in software |
| Sampled gas not fully mixed | Mixing tank upstream of sensor |

### Error: no reading
**Effects: Clinician denied information for setting Fi02**
| Causes | Controls |
|---|---| 
| Disconnected electrical contact | **No current control** |

- [ ] Approximate 02 settings marked on throttle, to allow clinican to set level i absence of readout

### High temperature (>300DegC) element ignites nearby components in high oxygen environment
**Effects: Fire. Patient smoke inhalation**
| Causes | Controls |
|---|---| 
| Intrinsic design fault: heated element placed too close to materials that could combust in high oxygen environment | **No current control** |

**Effects: Sensor may malfunction due to contact with molten plastic. Therefore error in oxygen reading**
| Causes | Controls |
|---|---| 
| Intrinsic design fault: heated element placed too close to materials that could combust in high oxygen environment  | **No current control** |

**Effects: Breach of gas circuit leading to ventilation failure**
| Causes | Controls |
|---|---| 
| Intrinsic design fault: heated element placed too close to materials that could combust in high oxygen environment  | low tidal volume alarm, low inhalation pressure alarm |

### Potential leak of contaminants to patient
**Effects: Patient exposed to contaminants**
| Causes | Controls |
|---|---| 
| Mishandling. Severe shocks. Sensor defective | Oxygen sensor on seperate bleed with positive pressure graident to outside. Therefore contaminants will not enter inhalation path. |

## References
FMEA paper from OVSI team
