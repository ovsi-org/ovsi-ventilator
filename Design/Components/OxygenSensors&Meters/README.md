# Oxygen Sensors and Oxygen Meters
Oxygen sensor are situated after the Air intake and O2 supply mixing, between the V-flow meter and the Inhale Solenoid valve.
They exist to check that the FiO2 delivered to the patient can be calculated and controlled.

![O2 sensors](O2sensor_chart.PNG)

Anything related to the risks and failures of these components can be found [here](Risks&Failures_galvanic.md) for the Galvanic Oxygen Sensor, [here](Risks&Failures_auto.md) for the automotive oxygen sensor, and [here](Risks&Failures_flometer.md) for the oxygen flow meter. 

The Oxygen flow meter is only needed if the oxygen sensors are not available (see [V-Flow meter page](https://gitlab.com/ovsi-org/ovsi-ventilator/-/tree/master/Design/Components/VFlowMeter)).

## Parts of the design that need changing
### Galvanic
- [ ] Use temperature and pressure data in calbiration
- [ ] Use temperature and pressure data in calbiration
- [ ] Create Error code on startup if oxygen concentration reads below atmospheric, indicating sensor beyond lifespan
- [ ] Approximate 02 settings marked on throttle, to allow clinican to set level i absence of readout
- [ ] Stress relief on solder connections
- [ ] Ensure transient response from change in oxygen setting is reasonable, and ensure that clinician is aware of delay ini oxygen reading.
- [ ] **Design controls for Lifespan of sensor**
- [ ] **Design controls for Disconnected electrical contact**

### Automotive lambda
- [ ] **Design control for Disconnected electrical contact**
- [ ] **Design control Intrinsic design fault: heated element placed too close to materials that could combust in high oxygen environment**

### Oxygen flowmeter
- [ ] potential failure effects and detection scores highly dependant on inclusion of O2 sensor
- [ ] Is a filter on the oxygen side something worth considering?
potential failure effects and detection scorse highly dependant on inclusion of O2 sensor.
- [ ] Need to understand sensitivity of flow meter to the orientation of the pipe assembly upstream. Will this be fixed for all assembled devices?
- [ ] Need to know if Po measured upstream of fow meter
- [ ] temperature measurement upstream of O2 flow meter
- [ ] If 02 sensor OR 02 flow meter read low then low 02 alarm should be triggered. If 02 sensor OR 02 flow meter high then high 02 alarm should be triggered. Then the most accurate sensor should be displayed, or an average of the two readings (TBD).
- [ ] Is a filter on the oxygen side something worth considering?
potential failure effects and detection scorse highly dependant on inclusion of O2 sensor
- [ ] size the flow meter to have sufficiently high Reynolds' number at low flow rates, include mass flow dependant discharge coefficient in algorithm that determines mass flow.
- [ ] ensure main gas path flow rate is much higer than oxygen flow rate so fire hazard is avoided

## Specifications

## Purchasing
All of these components are off-the-shelf
## References
OVSI team FMEA paper
