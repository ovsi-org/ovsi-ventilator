## Risks and Failures
This document shows the possible risks and failures of the galvanic type of O2 sensor. 

### Error: Fi02 reading erroneously high
**Effects: Clinician recieves innacurate feedback, consequently feeds the patient an Fi02 that is lower than intended**
| Causes | Controls |
|---|---| 
| Calibration error due to temperature and pressure changes. | Measurement of temperautre and pressure in gas circuit provide means to correct calibration in software |
| Sampled gas not fully mixed | Mixing tank upstream of sensor |

- [ ] Use temperature and pressure data in calbiration

### Error: Fi02 reading erroneously low
**Effects: Clinician recieves innacurate feedback, consequently feeds the patient an Fi02 that is higher than intended**
| Causes | Controls |
|---|---| 
| Calibration error due to temperature and pressure changes.  | Measurement of temperautre and pressure in gas circuit provide means to correct calibration in software |
| Sampled gas not fully mixed | Mixing tank upstream of sensor |
| Membrane obstructed due to dust or debris in gas circuit | Filtering upstream. Problem will be obvious as sensor always reads atmospheric oxygen concetration |
| Lifespan of sensor exceeded (typically 12-48 months, depeding on model) | **No current control for this** |

- [ ] Use temperature and pressure data in calbiration
- [ ] Create Error code on startup if oxygen concentration reads below atmospheric, indicating sensor beyond lifespan

### Error: no reading
**Effects: Clinician denied information for setting Fi02**
| Causes | Controls |
|---|---| 
| Lifespan of sensor exceeded (typically 12-48 months, depeding on model) | **No current control** |
| Disconnected electrical contact | **No current control** |

- [ ] Approximate 02 settings marked on throttle, to allow clinican to set level i absence of readout
- [ ] Stress relief on solder connections

### Error: Sensor undergoes transient response following rapid pressure or humidity change
**Effects: Clinican provided confusing information for setting Fi02. Clinician forced to wait while reading stabilises**
| Causes | Controls |
|---|---| 
| Rapid change of Oxygen (also potentialy Pressure, Temperature, humidity) | Gas circuit already filtered at inlet |

- [ ] Ensure transient response from change in oxygen setting is reasonable, and ensure that clinician is aware of delay ini oxygen reading.

### Leak of electrolyte and toxic material into circuit
**Effects: Patient exposed to contaminants**
| Causes | Controls |
|---|---| 
| Mishandling. Severe shocks. Sensor defective | Oxygen sensor on seperate bleed with positive pressure graident to outside. Therefore contaminants will not enter inhalation path. |

## References
FMEA paper from OVSI team
