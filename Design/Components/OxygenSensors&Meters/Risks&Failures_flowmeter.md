## Risks and Failures
This document shows the possible risks and failures of the Oxygen Flowmeter. Thi is only induced if oxygen sensor is not available, so the FMEA of this component is executed assuming there is not oxygen sensor in the system.

### Fails to measure correct oxygen mass flow rate, gives value that is **too high**. Amount of oxygen delivered to patient remains the same if throttle valve 1 is not changed
**Effects: The high oxygen alarm is triggered before oxygen level reaches the upper limit, so clinician could be lead to adjust throttle valve 1 to reduce oxygen flow unneccessarily
The low oxygen alarm is only triggered when the oxygen flow is already below that limit, so a clinician is not made aware of the levels of oxygen being too low
As gas composition not known precisely, innacurate value of density calculated downstream for V flow meter algorythm. This will lead to inacurate measurement of tidal volume.**
| Causes | Controls |
|---|---| 
| knock or drop leading leakage in the flow meter body, flow meter pipe connections or leakage in the pressure tappings | ventilator designed to withstand 1.5m drop. O2 sensor provides redundancy, no alarms without oxygen sensor |
| blocked or leaking pressure tappings due to debris ingestion reading pressure drop that is too large | using certified oxygen tanks or wall oxygen supply without contaminants. O2 sensor provides redundancy, no alarms without oxygen sensor |
| Sensitivity to upstream pipe orientation | no alarms without oxygen sensor |
| Change in Po,  leading to change in gas density | Po measurement upstream of flow meter |
| Change in To,  leading to change in gas density | To measurement upstream of flow meter |

- [ ] potential failure effects and detection scores highly dependant on inclusion of O2 sensor
- [ ] Is a filter on the oxygen side something worth considering?
potential failure effects and detection scorse highly dependant on inclusion of O2 sensor.
- [ ] Need to understand sensitivity of flow meter to the orientation of the pipe assembly upstream. Will this be fixed for all assembled devices?
- [ ] Need to know if Po measured upstream of fow meter
- [ ] temperature measurement upstream of O2 flow meter

### Fails to measure correct oxygen mass flow rate, gives value that is **too low**. Amount of oxygen delivered to patient remains the same if throttle valve 1 is not changed
**Effects: The low oxygen alarm is before oxygen level reaches the set limit, so clinician could be lead to adjust throttle valve 1 to increase oxygen flow unneccessarily
The high oxygen alarm is only triggered when the oxygen flow is already above that limit, so a clinician is not made aware of the levels of oxygen being too high.
As gas composition not known precisely, innacurate value of density calculated downstream for V flow meter algorithm. This will lead to inacurate measurement of tidal volume.**
| Causes | Controls |
|---|---| 
| knock or drop leading leakage in the flow meter body, flow meter pipe connections or leakage in the pressure tappings | ventilator designed to withstand 1.5m drop. O2 sensor provides redundancy, no alarms without oxygen sensor |
| blocked or leaking pressure tappings due to debris ingestion | using certified oxygen tanks or wall oxygen supply. O2 sensor provides redundancy, no alarms without oxygen sensor |
| both pressure tappings disconnected | sensor will read zero, no alarms without oxygen sensor |
| Change in Po, leading to change in gas density | Po measurement upstream of flow meter |
| Change in To, leading to change in gas density | To measurement upstream of the flow meter |
| Sensitivity to upstream pipe orientation | no alarms without oxygen sensor, sufficient upstream pipe length included in injection moulded component to ensure flow condition upstream of measurement |

- [ ] If 02 sensor OR 02 flow meter read low then low 02 alarm should be triggered. If 02 sensor OR 02 flow meter high then high 02 alarm should be triggered. Then the most accurate sensor should be displayed, or an average of the two readings (TBD).
- [ ] Is a filter on the oxygen side something worth considering?
potential failure effects and detection scorse highly dependant on inclusion of O2 sensor

### insufficient measurement accuracy at low flow rate
**Effects: inaccurate oxygen mass flow measurement at oxygen flow rate  , uncertainty of patient delivered oxygen fraction. As gas composition not known precisely, innacurate value of density calculated downstream for V flow meter algorithm. This will lead to inacurate measurement of tidal volume.**
| Causes | Controls |
|---|---| 
| sensitivity of discharge coefficient at low Reynolds' number | **none without oxygen sensor** |

- [ ] size the flow meter to have sufficiently high Reynolds' number at low flow rates, include mass flow dependant discharge coefficient in algorithm that determines mass flow.

### non-sensical O2 reading
**Effects: no reading available for user**
| Causes | Controls |
|---|---| 
| one pressure tap not connected | O2 sensor still reads O2 fraction, no alarms without oxygen sensor |

### blocks oxygen flow
**Effects: only air is delivered to patient**
| Causes | Controls |
|---|---| 
| knock or drop causes complete junction disconnection or shattering of component | ventilator designed to withstand 1.5m drop. O2 sensor provides redundancy, no alarms without oxygen sensor |

### Depressurisation
**Effects: Ventilation failure**
| Causes | Controls |
|---|---| 
| Knock/drop leading to disconnect of connectors | Low tidal volume alarm will sound, low pressure alarm will sound. |

### Depressurisation
**Effects: Oxygen leak - fire hazard**
| Causes | Controls |
|---|---| 
| Knock/drop leading to disconnect of connectors | Low tidal volume alarm will sound, low pressure alarm will sound. |
- [ ] ensure main gas path flow rate is much higer than oxygen flow rate so fire hazard is avoided
### Leaks
**Effects: Oxygen leak - fire hazard**
| Causes | Controls |
|---|---| 
| pressure hose disconnecting due to knock or drop | ventilator designed for 1.5m drop, no alarm without oxygen sensor |
| knock or drop causes fracture of component | ventilator desinged for 1.5m drop, no alarm without oxygen sensor |

- [ ] ensure main gas path flow rate is much higer than oxygen flow rate so fire hazard is avoided
## References
FMEA paper from OVSI team
