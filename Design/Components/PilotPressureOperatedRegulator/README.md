# Pilot operated pressure regulator

[[_TOC_]]

## Overall

The Pilot operated pressure regulator ensures oxygen enters the main flow path at the pilot pressure set by the [throttle valve](../ThrottleValve). It is situated between the oxygen input and the mixing tank (called'pressure switch' in the diagram).

![Pressure switch](switch_flowchart.PNG "Pressure switch")

Anything related to the possible the risks and failures of the Pilot operated pressure regulator can be found in the [Risks and Failures](Risks&Failures.md) document in this folder.


### Location
- It operates directly downstream of the pressure regulated oxygen supply inlet
    - This inlet delivers pure oxygen at a gauge pressure of 10kPa. See [note](#specification) in specification.

- [ ] **Where is the oxygen supply inlet and the Pilot operated pressure regulator on the schematic?**

### Parts of the design that need changing
Rationale for these can be found in the [risks and failures](Risks&Failures.md) document of this folder.
- [ ] fatigue life analysis of diaphragm, proof testing of diaphragm life, turn switch upside down so plunger defaults to closed under gravitiy.
- [ ] check the materials, manufacture and strength of diaphragm. 
- [ ] Prove life of component and include in service manual what the service interval is for the diaphragm.



### Functioning
- A diaphragm, with one side at the pilot pressure and the other at the main gas path pressure, controls the flow of oxygen.
    - The pressure difference across the diaphragm is controlled by the setting of the throttle valve.
        - *This pressure difference is between the pilot pressure and the main gas path pressure at the exit of the pilot pressure operated regulator.*

- [ ] **How is the diaphragm and the throttle valve controlled?**

**Two limiting cases of the throttle**

| Limiting case | Action | What is delivered to the patient? |
|---|---|---|
| Completely *closed* throttle valve | The Pilot operated pressure regulator releases oxygen at the [pressure relief valve](../PressureReliefValve) pressure. | 100% oxygen is delivered to the patient.  |
| Completely *open* throttle valve | There is an insignificant pressure drop between the pilot pressure and main gas path pressure at the exit of the Pilot operated pressure regulator so the diaphragm is not stretched and the regulator remains closed.  | Only pressurised air is delivered to the patient. |

- In between these two settings, the Pilot operated pressure regulator in conjunction with the throttle valve controls the amount of oxygen that is added to the main gas path and delivered to the patient. [^1]

- [ ] **How does the operator know how much oxygen that should be added to the main gas path?**

### Drawings
![Pilot pressure operated regulator](https://docs.google.com/drawings/d/e/2PACX-1vQ_ax2-khlO1_rPH6XWKX4B-cCZ5_nryIc3uvUXuadDNi243VOWtCdskwJhS9N7eV2cBtzjBaC78fDn/pub?w=960&h=720 "Cross-Section of Pilot operated pressure regulator")
*Cross-section of the Pilot operated pressure regulator showing the pilot pressure inlet (top right), oxygen inlet (bottom right) and oxygen outlet to main gas path (bottom left). The diaphragm operated valve separates the oxygen from the main gas path.*

- [ ] **Where is the diaphragm and the diaphragm operated valve on this diagram? Are there any other important details?**

## Specification

| Component | Metric | Value | Status | Notes |
|---|---|---|---|---|
| Pilot operated pressure regulator | Gauge pressure operating range | 1.5 - 10 kPa | Requirement | |
| External pressure regulator | Deliver oxygen at gauge pressure | 10 kPa | Requirement | Outside the scope of this ventilator design |

- [ ] **What are the tolerances for these values?**

## Manufacture

- [ ] **Need to add manufacturing details**

### 1. Fabrication

### 2. Purchasing

## Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
