## Risks and Failures
This document shows the possible risks and failures of the Pilot Operated Pressure Regulator. 

### fails to close at set pressure
**Effects: constant supply of O2, wasting O2 through the safety valve downstream, potential overpressuriation of patient if PIP is set below the safety relief valve pressure.**
| Causes | Controls |
|---|---| 
| debris blocking valve closure | inlet and viral filters upstream, high oxygen alarm, high pressure alarm likely |
| drop or knock damaging mechanism | ventilator designed for 1.5m drop, high oxygen alarm |
| diapraghm fatigue leads to plunger disconnecting | high oxygen alarm triggered by O2 sensor |
| broken mechanism due to excessive wear | high oxygen alarm |

- [ ] fatigue life analysis of diaphragm, proof testing of diaphragm life, turn switch upside down so plunger defaults to closed under gravitiy.

### leaks internally
**Effects: constant supply of O2, wasting O2 through the safety valve downstream. PIP and oxygen concentration can no longer be set independently because pilot operated pressure regulator does not react to the pilot pressure as intended**
| Causes | Controls |
|---|---| 
| debris blocking valve closure, broken moving parts | inlet and viral filters upstream, high oxygen alarm |
| worn out mechanism | certified pressure switch, oxygen alarm |
| drop or knock | ventilator designed for 1.5m drop, oxygen alarm |

### depressurisation of oxygen side
**Effects: higher oxygen concentration in box, wasting oxygen**
| Causes | Controls |
|---|---| 
| drop or knock | box is ventilated, oxygen diluted. low oxygen alarm  |

### depressurisation of main gas path
**Effects: Ventilation failure**
| Causes | Controls |
|---|---| 
| Damage due to knock or drop. | ventilator is designed to withstand 1.5m drop, uses ductile materials to prevent fracture, low tidal volume alarm, low PIP alarm |


### blockage of orifice
**Effects: reduced O2 concentration at fixed flow rate or reduced flow rate at increased O2 concentration**
| Causes | Controls |
|---|---| 
| debris in O2 supply | low tidal volume alarm or low oxygen alarm |

### fails to open at set pressure
**Effects: only air can be supplied to patient, no extra oxygen supply**
| Causes | Controls |
|---|---| 
| debris jamming switch mechanism | inlet and viral filter upstream, oxygen flow meter reads zero, oxygen alarm from oxygen sensor |
| pilot pipe completely blocked due to debris, so valve no longer opens when set pressure is reached | inlet and viral filter upstream, oxygen flow meter reads zero, oxygen alarm from oxygen sensor |
| pilot pipe disconnected due to drop or knock, so pressure switch never opens | ventilator designed to withstand 1.5m drop, oxygen flow meter reads zero, oxygen alarm from oxygen sensor |
| diaphragm burst due to material deterioration, fatigue or overpressure. Sensitivity of material properties to temperature can make the diaphragm's mechanical behaviour transition from ductile to brittle | suitable material choice. low oxygen alarm |
| pilot pipe leaks or partially blocked so pressure switch opens when pressure is higher than set pressure | inlet and viral filter upstream, oxygen flow meter reads zero, oxygen alarm from oxygen sensor |

- [ ] check the materials, manufacture and strength of diaphragm. 
- [ ] Prove life of component and include in service manual what the service interval is for the diaphragm.


## References
FMEA paper from OVSI team
