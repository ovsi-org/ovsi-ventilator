# Connectors

Contains a wide variety of connectors in different formats (stl, stp and step). 45 degree connectors, 4-way fitting, elbow connector, T connector, E adaptor, and S-duct are standard tubing parts. 

Airflow pipe, inline filter, O-flow and oxflow pipe are linear fitting which either reduce the diameter of the pipe or the volume of the airflow, or act as filters.

![](connector_chart.PNG)

## Airflow pipe

## Inline filter
*I suppose They have to be filled with a filter ? REFERENCE ?*

## o-flow

## oxflow pipe
Is where the respirator is connected to an oxygen supply (oxygen concentrator *Is it designed for a concentrator? Can it be used with a compressed oxygen+valve system? What pressure/flow can the system take in?*)

## Specifications

* **Oxygen compatibility considerations? Assume yes for all?**
* **Material needed?**
* **Medical grade? Assume yes for all**
* **Dimater?**


## Fabrication/purchasing

**Is this part fabricated or purchased? - Details of fabrication?**
**I notice the BOM has "T piece for big diameter".  Are there multiple diameters? What are they?**

<!-- Step files of the [elbow connector](elbow_connector.stp) and [tee connector](tee_connector.stp) are available. ** Where are the original CAD files?** -->

