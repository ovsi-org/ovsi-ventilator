## Risks and Failures
This document shows the possible risks and failures of the Compressor Assembly situated between the Air Intake and Safety Relief Valve #1.

### Fails to provide any pressure rise
**Effects: Ventilation Failure**
| Causes | Controls |
|---|---| 
| Power cut at facility | Backup battery supply |
| Power cable removed | If removed before battery supply backup batteries can be used, otherwise nothing |
| Power wires disconnected from motor | Component has suitable mean time to failure |
| Compressor rotor/motor seizes. Inlet blocked enough to lead to compressor stall. | Component has suitable mean time to failure. Volute sized so as to avoid tip rubbing. Low tidal volume alarm will sound. |

### Gives too high of a pressure rise
**Effect: Patient recieves air at too high of a pressure and flowrate of air is too low (see change of operating point on compressor characteristic)**
| Causes | Controls |
|---|---| 
| Inlet partially blocked e.g. by sheet or towel, throttling the compressor and moving it up the characteristic | Pressure regulation valve keeps pressure at correct value, additionally two safety releif valves prevent damage to patient. Stall pressure ratio not significantly higher than design operating point. Low tidal volume alarm will sound if flow rate becomes dangerously low |

**Effect: Reduction in motor cooling flow could lead to overheating**
| Causes | Controls |
|---|---| 
| Inlet partially blocked e.g. by sheet or towel, throttling the compressor and moving it up the characteristic | Temperature monitoring |

### Foreign objects/substances entering airflow
**Effect: *Patient gas path is contaminated with biologically harmful substance*
| Causes | Controls |
|---|---| 
| An air path that pushes grease from bearing into main gas path | Sealed bearings, no known air path that could do this exists. Downstream filter |
| Tip rubbing | Suitable design to give tip clearance. Motor cooling bypass to avoid heat expansion |
| Large particulate entry | Inlet filter |
| Particulates from motor being released | Brushless motor was used |

**Effect: *Overheating of motor*
| Causes | Controls |
|---|---| 
| Bearing lubrication reduced due to leakage | Temperature monitoring and check valve. Downstream viral filter should stop smoke reaching patient |

### Makes excessive noise
**Effect: *Discomfort is felt by patient*
| Causes | Controls |
|---|---| 
| Blade damage | Inlet filter |
| Tip rubbing | Suitable design to give tip clearance. Motor cooling bypass to avoid heat expansion |
| Rotor out of balance due to particulate accumulation or damage | Inlet filter |
| Leakage of air | Hard external box, component surroundings packed with foam to reduce handling damage |
| Insufficient sound-proofing of motor | Sits in closed box |

**Effect: *Communication near patient is inhibited*
| Causes | Controls |
|---|---| 
| Blade damage | Inlet filter |
| Tip rubbing | Suitable design to give tip clearance. Motor cooling bypass to avoid heat expansion |
| Rotor out of balance due to particulate accumulation or damage | Inlet filter |
| Leakage of air | Hard external box, component surroundings packed with foam to reduce handling damage |
| Insufficient sound-proofing of motor | Sits in closed box |

### System leaks
**Effect: *Flow rate of system is reduced*
| Causes | Controls |
|---|---| 
| Droppage | Hard external box, component surroundings packed with foam to reduce handling damage. Low tidal volume alarm will sound |
| Tip rubbing | Suitable design to give tip clearance. Motor cooling bypass to avoid heat expansion. Low tidal volume alarm will sound |
| Overheating | Motor cooled with motor cooling bypass, temperature of motor monitored. Low tidal volume alarm will sound |
| Volute hit | Hard external box, component surroundings packed with foam to reduce handling damage. Low tidal volume alarm will sound |
| Sealant/adhesive failure | Low tidal volume alarm will sound |

### System depressurises
**Effect: *Patient does not receive ventilation*
| Causes | Controls |
|---|---| 
| Droppage | Hard external box, component surroundings packed with foam to reduce handling damage. Pressures are monitored |
| Tip rubbing | Suitable design to give tip clearance. Motor cooling bypass to avoid heat expansion. Pressures are monitored |
| Overheating | Motor cooled with motor cooling bypass, temperature of motor monitored. Pressures are monitored |
| Volute hit | Hard external box, component surroundings packed with foam to reduce handling damage.  Pressures are monitored |
| Sealant/adhesive failure | Pressures are monitored |

### Compressor/Motor catches fire
**Effect: *Fire - patient and those around them are at severe risk of death*
| Causes | Controls |
|---|---| 
| Overheating | Motor cooled with motor cooling bypass, temperature of motor monitored |
| Bearing damage | Inlet filter |
| Lubrication leak | Sealed bearings, no known air path that could push grease into air path exists |
| Tip rubbing | Suitable design to give tip clearance. Motor cooling bypass to avoid heat expansion |

## References
FMEA paper from OVSI team
