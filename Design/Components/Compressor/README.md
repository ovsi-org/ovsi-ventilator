# Compressor Assembly

The centrifugal compressor supplies the pressure rise to the system required to drive the air flow to the patient.

The compressor assembly consists of the following components:
1. Flow inlet with filter - The inlet allows air to enter the system and includes a filter to remove large particles and debris which could cause damage to the compressor.
2. Centrifugal compressor - The centrifugal compressor supplies the pressure rise to the system required to drive the air flow to the patient.
3. Volute
4. Compressor coolant bypass

- The flow exits through a diffuser built into the compressor-motor assembly and is collected in the volute.
- 30% of the flow is then diverted away from the main gas path and over the motor electronics in order to cool them.
    - The temperature of these components is monitored for safety purposes.
    - The remainder of the flow is delivered to the downstream system.

![](compressorassembley.PNG)

## Parts of the design that need changing

## Drawings
![Compressor Assembly](Compressor.png "Cross-Section of Compressor Assembly")
*Cross-section of the compressor assembly, showing the inlet, centrifugal compressor-motor and volute*

## Specifications
- Total-to-total pressure rise: 1.08 
    - At flow rate: 250 l/min 
    - At altitude above sea level: 3000m
- The pressure rise decreases to 1.06 at sea level — sufficient for all operating conditions and modes of the ventilator. 

## Purchasing

- Brushless DC vacuum motor
    - For the ventilator to be manufactured in-country around the world it was necessary that the compressor-motor assembly be readily available.
    - The choice of a brushless motor also alleviates the problem of carbon particles from brushes entering the airstream. 

## Sources

[1] [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive
