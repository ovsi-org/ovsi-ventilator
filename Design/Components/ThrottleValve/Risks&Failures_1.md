## Risks and Failures
This document shows the possible risks and failures of the throttle valve located after Check Valve #1 and between the Pressure Switch and the Mixing Tank.

### Throttle getting stuck 
**Effects: Unable to change Fi02, leading to delivery of higher or lower than desired Fi02 to patient**
| Causes | Controls |
|---|---| 
| Distortion of mechanism due to rouch handling | None |
| Handle breakage due to Dropped from upto 1.5m | None |

### Mis-match between marked throttle setting and handle position 
**Effects: Misset value of Fi02 (low or high)**
| Causes | Controls |
|---|---| 
| Handle loose on shaft - due to wear/becoming coupling looseing with use | 02 sensor  |

### Valve position can change without user interaction 
**Effects: FiO2 not set correctly and may change over time**
| Causes | Controls |
|---|---| 
| Ventilator dropped | O2 concentration monitored so if in wrong position for patient this will be alerted via alarms. Hard case and foam around components |
| Worn or badly formed threads | Threads are large to make robust, flow rate alarm and pressures are monitored.  |

### Flowrate too sensitive to changes in valve position over relevant range of flowrates 
**Effects: Very difficult to set correct FiO2. May end up with large changes in flow rate to patient**
| Causes | Controls |
|---|---| 
| Non-linearity of valve position to pressure loss | Proper design of valve for flowrate range |
| Damage to valve plate from debris | Upstream filters |

### Flowrate not sensitive enough to changes in valve position over relevant range of flowrates 
**Effects: Very large turns on valve required to change FiO2, may end up over adjusting as valve becomes more sensitive in middle of range**
| Causes | Controls |
|---|---| 
| Non-linearity of valve position to pressure loss | Proper design of valve for flowrate range |
| Damage to valve plate from debris | Upstream filters |

### Partial blockage 
**Effects: Delivery of higher than desired Fi02 to patient. Low air flowrate leads to high Fi02 to compensate**
| Causes | Controls |
|---|---| 
|  |  |

### Complete blockage  
**Effects: Delivery of 100% Fi02 to patient. Total cuttoff of air flowrate, leading to pressure switch compensating by providing 100% oxygen**
| Causes | Controls |
|---|---| 
| Debris in gas circuit;
Jammed shut | 02 sensor. Intake and viral filter upstream |

### Depressurisation  
**Effects: Ventilation failure due to loss of delivery pressure**
| Causes | Controls |
|---|---| 
| Rough handling, shocks | Low tidal volume alarm will sound, low pressure alarm will sound |
| Incorrect installation of connectors | Low tidal volume alarm will sound, low pressure alarm will sound |
| Wear | Low tidal volume alarm will sound, low pressure alarm will sound |

### Leaks to outside of gas path 
**Effects: Pilot pressure would be lower than desired value and oxygen would be reduced at a lower than desired pressure reducing FiO2. Also reduced flow to patient**
| Causes | Controls |
|---|---| 
| Rough handling, shocks | Hard case, Low tidal volume alarm, low oxygen alarm, low pressure alarm |
| Incorrect installation of connectors | Low tidal volume alarm, low oxygen alarm, low pressure alarm |
| Wear | Low tidal volume alarm, low oxygen alarm, low pressure alarm |

### Leaks intenally  
**Effects: Throttle appears more open than it is set to, can be accounted for unless trying to run 100% oxygen**
| Causes | Controls |
|---|---| 
| Rough handling, shocks | Hard case, if throttle leaks more over time a low oxygen alarm will sound, if set while leaking no alarm will sound but won't be able to reach 100% oxygen |
| Incorrect installation of connectors | if throttle leaks more over time a low oxygen alarm will sound, if set while leaking no alarm will sound but won't be able to reach 100% oxygen |
| Wear | if throttle leaks more over time a low oxygen alarm will sound, if set while leaking no alarm will sound but won't be able to reach 100% oxygen |
| Debris stopping full throttle closure | if throttle leaks more over time a low oxygen alarm will sound, if set while leaking no alarm will sound but won't be able to reach 100% oxygen. Debris traps and filters |

## References
FMEA paper from OVSI team
