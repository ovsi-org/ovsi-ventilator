## Risks and Failures
This document shows the possible risks and failures of the throttle valve located between the Mixing Tanks and Safety Relief Valve #2.

### Throttle setting stuck
**Effects: Unable to change the flowrate. If stuck in high loss setting flowrate to patient will be too low. If stuck in low loss setting flowrate to patient will be too high**
| Causes | Controls |
|---|---| 
| Ventilator dropped | Pressures and flowrate monitored so if locked in wrong position for patient this will be alerted via alarms. Hard case and foam around components |
| Threads jammed | Very large threads with a lead in. No need to completely unscrew |

### Mis-match between marked throttle setting and handle position
**Effects: Misset flowrate (low or high)**
| Causes | Controls |
|---|---| 
| Handle loose on shaft - due to wear/becoming coupling looseing with use | Tidal volume alarm |

### Valve position can change without user interaction
**Effects: Flowrate not set correctly and may change over time**
| Causes | Controls |
|---|---| 
| Ventilator dropped | Pressures and flowrate monitored so if in wrong position for patient this will be alerted via alarms. Hard case and foam around components |
| Worn or badly formed threads | Threads are large to make robust, flow rate alarm and pressures are monitored |

### Flowrate too sensitive to changes in valve position over relevant range of flowrates
**Effects: Very difficult to set correct flow rate. May end up with large changes in flow rate to patient**
| Causes | Controls |
|---|---| 
| Non-linearity of valve position to pressure loss | Proper design of valve for flowrate range |
| Damage to valve plate from debris | Upstream filters |

### Flowrate not sensitive enough to changes in valve position over relevant range of flowrates
**Effects: Very large turns on valve required to change flowrate, may end up over adjusting as valve becomes more sensitive in middle of range**
| Causes | Controls |
|---|---| 
| Non-linearity of valve position to pressure loss | Proper design of valve for flowrate range |
| Damage to valve plate from debris | Upstream filters |

### Partial blockage
**Effects: Flow rate reduced, possibly leading to possibly insufficient control over flowrate**
| Causes | Controls |
|---|---| 
| Ingress of debris | Upstream filters, low tidal volume alarm |

### Complete blockage
**Effects: Flow rate reduced, possibly insufficient flow to patient. Loss of pressure**
| Causes | Controls |
|---|---| 
| Ingress of debris | Upstream filters, low tidal volume alarm |

### Depressurisation of system
**Effects: Patient does not recieve ventilation. If everything downstream ofthrottle goes to atmospheric pressure, when exhale solenoid opens backflow would occur due to the higher PEEP maintained there. If Y-piece has check walves this won't happen**
| Causes | Controls |
|---|---| 
| Dropping, improper handling | Low tidal volume alarm and low pressure alarm.
hard external case |
| Ingress of debris | Upstream filter |

### Leaking externally
**Effects: Reduced flow rate of system**
| Causes | Controls |
|---|---| 
| Dropping, improper handling | Hard external case |
| Improper fitting of connectors | Gas connectors have ribbed friction grips and seals. Difficult to fit incorrectly and give a good seal |
| Vibration |  |

**Effects: Effect of throttle valve is reduced - lungs will inflate quicker than desired. If this is small leak then the throttle valve can be adjusted to alieviate this problem**
| Causes | Controls |
|---|---| 
| Dropping, improper handling | Hard case, high flow rate alarm |
| Improper fitting of connectors | High flow rate alarm |
| Vibration | High flow rate alarm |

## References
FMEA paper from OVSI team
