# Throttle Valve
## 1. Overall
Throttle valves are used in two places within the ventilator assembly **(Why is there only one listed in the main design diagram?)** with the function of each being distinct. The first throttle valve controls the flow rate of air entering the Air-O2 section of pipework and also sets the pressure of the pilot to the Pilot Operated Pressure Regulator. This throttle valve, in conjunction with the Pilot Operated Pressure Regulator therefore adjusts the FiO2 delivered to the Patient. The second throttle valve, located downstream, adjusts the flow rate of the Air-O2 mix delivered to the Patient and therefore the time constant for lung inflation. 

A piston type valve was chosen for both throttle valves due to it’s simple design and robustness. They consist of a threaded cylindrical nut that can be raised out of or lowered into the path of the flow in order to decrease or increase the pressure loss, therefore changing the flow rate. The valves have a range between fully open (where the piston does not enter the flow path) to fully closed (where the piston completely closes the flow path) in which state no flow can pass through the valve.

### 1.1 Location

- The throttle valve inlet is from the mixing tank and outlet to the second safety relief valve.
## 2. Design

### 2.1 Features

| Feature | Function | Effect |
|---|---|---|
| | | |

### 2.2 Functioning

| State | Action | Reason |
|---|---|---|
| | | |
| | | |

### 2.3 Drawings
![](CrossSectionThrottleValve.png)  
Cross section of the throttle valve

## 3. Specification

* Throttle flow: From no restriction (no pressure drop - max flow rate) to closed.
* Type: manual gate type valve
* Medical grade?: Yes
* 100% oxygen compatible: Yes

## 4. Manufacture

The part could be fabricated or purchased.

### 4.1 Fabrication
**What methods are recommended?**

A [step file of the throttle valve is provided in this folder](throttle_straight_asm.stp) **Where is the original CAD file?**

### 4.2 Purchasing

* Supplier: **Need example supplier**
* Part no: **Need example part number**
* Link: **Cannot find link**

## 5. Sources

[^1]: [Technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) on OVSI Google Drive






