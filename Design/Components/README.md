# Components
THis section lists all the mechanical components involved in the Whittle 2 design, withe its specifications, requirements, files if they are to be manufactured, and possible risks and failures.
This is what the prototype should look like when assembled.

![](fullW2_picture.PNG)

# Disclamier

For each part that is manufactured and not bought, a risk assessment ha to be performed to assess the material safety in a medical device setting. This includes Toxicity, Genotoxicity and Carcenotoxocity of the materials involved, as well as reproductive and developmental toxicity and degadation of the material.

(ref, ISO 10993-1:2018 Biological evaluation of medical devices — Part 1: Evaluation and testing within a risk management process)

<!--Unfortunately, ISO standards are not open source, and therefore we can only summarize their conclusions and not include them here.-->
A major component in hazard identification is material characterization (see ISO 10993-18 and ISO/TR 10993-19). The following steps can be identified:

* define and characterize each material, including suitable alternative materials;
* identify hazards in materials, additives, processing aids, etc.;
* identify the potential effect of downstream processing (e.g. chemical interactions between material components, or final product sterilization) on chemicals present in final product;
* identify the chemicals that could be released during product use (e.g. intermediate or final degradation products from a degradable implant);
* estimate exposure (total or clinically available amounts);
* review toxicology and other biological safety data (published/available).

Information on biological safety to be reviewed can include:

* toxicology data on relevant component materials/compounds;
* information on prior use of relevant component materials/compounds;
* data from biological tests.

The risks posed by the identified hazards should then be evaluated. At this stage it should be possible to determine whether there is an undue toxicological risk from the material.

# List of components

Based on the [technical white paper](https://docs.google.com/document/d/1EWJrY87E6ham0mUXZFAKa_kr6ECdVW_AVwlc7W973N0/edit) in the Google Drive
