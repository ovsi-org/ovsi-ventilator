## Risks and Failures
This document shows the possible risks and failures of the Emergency Inhale Check Valve.
### Does not open
**Effects: no single failure effect. This valve is introduced to allow patient to breathe after loss of power or ventilation. If this valve does not open after ventilation loss, the severity is very high**
| Causes | Controls |
|---|---| 
| check valve size too large/heavy to open for application | adequate valve sizing |
| valve opening blocked by debris | inlet filter and debris traps upstream |
| sticktion between valve seal and valve | adequate valve selection for high oxygen and long closure times |

### Leaks to outside of gas path
**Effect: reduced tidal volume and inhalation pressure**
| Causes | Controls |
|---|---| 
| leak through valve seal | low PIP alarm |
| leak through housing or pipe junction due to knock or drop | low PIP alarm |

inhale check valve is essential to prevent loss of PEEP

### causes complete depressurisation of ventilator
**Effect: Ventilation failure**
| Causes | Controls |
|---|---| 
| Knock or drop | low PIP alarm, low tidal volume alarm, ventilator is designed to withstand 1.5m drop |

**Effect: introduction of debris to flow path**
| Causes | Controls |
|---|---| 
| Knock or drop | HME filter in patient mask, low PIP alarm, low tidal volume alarm,ventilator is designed to withstand 1.5m drop |

Ensure PEEP valve does not prevent patient inhaling at ambient pressure after loss of pressurisation. Introduce valve on exhaust system that opens after ventilation failure to bring down the exhale pressure to atmospheric and allow patient to breathe in and out
Screen reader support enabled.
 

## References
FMEA paper from OVSI team
