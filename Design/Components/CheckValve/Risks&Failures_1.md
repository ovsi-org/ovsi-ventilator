## Risks and Failures
This document shows the possible risks and failures of the Check Valve situated between the air reservoir and the O2 supply. 

### Fails to prevent backflow
**Effects: Wasting O2 in safety relief valve when inhale solenoid valve is closed**
| Causes | Controls |
|---|---| 
| Breakage of valve moving parts | Off the shelf component to be used (medical grade) |
| Excessive wear due to valve flutter because valve is too big |  Ensure correct sizing of check valve to avoid valve flutter |
| Too slow to close due to excessive valve size | Ensure correct sizing of check valve to avoid valve closure lag |

**Effect: Backflow of potentially O2 rich air to upstream components increases risk of corrosive damage**
| Causes | Controls |
|---|---| 
| Breakage of valve moving parts | Off the shelf component to be used (medical grade) |
| Excessive wear due to valve flutter because valve is too big | Ensure correct sizing of check valve to avoid valve flutter |
| Too slow to close due to excessive valve size | Ensure correct sizing of check valve to avoid valve closure lag |
### Fails to allow forward flow
**Effect: No flow of pressurised air to patient, ventilation failure**
| Causes | Controls |
|---|---| 
| Ingress of dirt/debris in mechanism | Gas circuit filtered at inlet, Low tidal volume alarm, Patient can still breathe through emergency inhale check valve |
| large foreign object in flow path | air filter at inlet, Low tidal volume alarm, Patient can still breathe through emergency inhale check valve |
### Excessive pressure drop
**Effect: Ventilation failure, if pressure drop signicant enough that pressure relief valve cannot compensate**
| Causes | Controls |
|---|---| 
| Ingress of dirt/debris in mechanism | Gas circuit filtered at inlet, Low tidal volume alarm, Patient can still breathe through emergency inhale check valve |
| large foreign object in flow path | air filter at inlet, Low tidal volume alarm, Patient can still breathe through emergency inhale check valve | 
### Fails to allow flow in either direction
**Effect: No pressurisation of system, ventilation failure**
| Causes | Controls |
|---|---| 
| ingress of contaminants leading to valve sticking shut | inlet and HEPA filter upstream, Low tidal volume alarm, Patient can still breathe through emergency inhale check valve |
| Breakage of valve moving parts | Off the shelf component to be used (medical grade), Patient can still breathe through emergency inhale check valve |

- [ ] move pressure pilot line to upstream of the check valve and throttle valve so O2 can compensate for lack of air flow

## References
FMEA paper from OVSI team

