## Risks and Failures
This document shows the possible risks and failures of the Check Valve linked to the Inhale solenoid valve. 
### Fails to prevent backflow
**Effects: Contamination of device with exhaust gas, leading to growth of pathogens in circuit and potential contamination of patient**
| Causes | Controls |
|---|---| 
| Breakage of valve disk/diaphragm due to shock drop | inhalation solenoid valve provides second barrier to upstream contamination. |
| Wear of moving compoennts |  inhalation solenoid valve provides second barrier to upstream contamination. |
| Movement of diaphragm blocked by mould | inhalation solenoid valve provides second barrier to upstream contamination. |

In light of the current controls, it is worth considering having a viral filter on inhalation leg of Y peice

### Partial blockage
**Effect: Reduced flow rate and inhalation pressure; partial ventilation failure**
| Causes | Controls |
|---|---| 
| Ingress of dirt/debris from upstream | Gas circuit filtered at inlet, Low tidal volume alarm. Low PIP alarm if PIP measurement occurs downstream of check valve |
| Movement of diaphragm blocked by mould | Gas circuit filtered at inlet, Low tidal volume alarm. Low PIP alarm if PIP measurement occurs downstream of check valve |

### Blockage
**Effect: Ventilation failure**
| Causes | Controls |
|---|---| 
| Ingress of dirt/debris from upstream | Gas circuit filtered at inlet, Low tidal volume alarm. Low PIP alarm if PIP measurement occurs downstream of check valve |
| Movement of diaphragm blocked by mould | Gas circuit filtered at inlet, Low tidal volume alarm. Low PIP alarm if PIP measurement occurs downstream of check valve |

### Leakage/depressurisation
**Effect: Ventilation failure; environment potentialy contaminated with exhalation gas**
| Causes | Controls |
|---|---| 
| Incorrect connector installation | Low PIP alarm, Low tidal volume alarm, PEEP alarm |
| Fracture due to shock/droppage | Low PIP alarm, Low tidal volume alarm, PEEP alarm |

### Harbours/allows growth of pathogens or mould (e.g. legionella), which are subsequntly released into gas circuit downstream
**Effect: Contamination of patient**
| Causes | Controls |
|---|---| 
| Check valve is exposed to exhalation gas | **No current control** |

## References
FMEA paper from OVSI team
