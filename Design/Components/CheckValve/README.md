# Check valve

The purpose of a check valve is to only allow airflow in one direction. It therefore prevents backflow in the gas circuit by blocking the reverse flow, in this case with a membrane. 

In the OVSI ventilator, one check valve is located between the mixing tank and variable oxygen supply (Check valve 1). Backflow must be avoided in this location, to prevent highly oxygenated gas from flowing backward in the system and exposing upstream components to high oxygen concentrations, which would require special design. Backflow could also result in oxygen leakage via the pressure relief valve, creating a fire hazard.

Two other check valves are situated at each end of the patient Y piece, to ensure that only the inhalation flow goes from the inhale solenoid valve and into the patients lungs and only the exhalation gases comes out of it and into the exhale solenoid valve (Check valves 2).

At last, the operation of the check valve is the same as the emergency inhalation check vent, and so the two devices are identical. (Check Valve 3) A standard diaphragm check valve is used for this purpose.

![](checkvalves.PNG)

In this design, the file "[check valve asm](check_valve_asm.stp)" is a full 3D rendering of the check valve with its membrane (and is not to be printed). **Check Valves must not be printed and should be bought off the shelf**

The reasoning for this decision can be found in the [Risks and Failures](Risks&Failures_1.md) part of this directory. 
Possible Failures, their causes and controls are listed in [Risks&Failures_1](Risks&Failures_1.md) for the check valve 1, [Risks&Failures_2](Risks&Failures_2.md) for check valves 2, and [Risks&Failures_3](Risks&Failures_3.md) for the emergency inhalation check vent.

## Parts of the design that need changing
**The check valves cannot safely be manufactured. These parts must be bought.**

- [ ] Ensure PEEP valve does not prevent patient inhaling at ambient pressure after loss of pressurisation. Introduce valve on exhaust system that opens after ventilation failure to bring down the exhale pressure to atmospheric and allow patient to breathe in and out.

- [ ] Worth considering having a viral filter on inhalation leg of Y piece

- [ ] Move pressure pilot line to upstream of the check valve and throttle valve so O2 can compensate for lack of air flow

## Specifications

* Minimal pressure drop: <100 Pa at 160 l/min
* Diaphraghm diameter: 20-25 mm
* Medical grade?: Yes
* 100% oxygen compatible: Yes

## Purchasing
The part should not be fabricated and should be purchased instead. Any material used need to be medical grade check valves, and be able withstand prolonged exposure to oxygen and heat < 140°C. 

* Part name : Ambu One Way Valve
* Supplier example : AHP Medicals; Part no: 252153
* Link: https://www.ahpmedicals.com/ambu-one-way-valve-for-the-rescue-mask-1-12334.html or 
https://www.worldpoint.com/rescue-mask-valve-w-filter
* Price : about £1 per piece.

### Cleaning/maintenance
Instructions for cleaning and maintenance can be found in the user manual for the Hamilton check valves [ [1] ](https://www.hamilton-medical.com/dam/jcr:e9d3590e-15c5-4e4f-bb3b-351ce0f2b116/Expiratory-valve-set-autoclavable-reprocessing-guide-de-en-fr-it-es-pt-ru-tr-zh-624591.07.pdf)

### References
[1] Hamilton Medical, Autoclavable Expiratory Valve Set Reprocessing Guide https://www.hamilton-medical.com/dam/jcr:e9d3590e-15c5-4e4f-bb3b-351ce0f2b116/Expiratory-valve-set-autoclavable-reprocessing-guide-de-en-fr-it-es-pt-ru-tr-zh-624591.07.pdf
