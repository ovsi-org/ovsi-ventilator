# Regulations

The regulations and requirements for this ventilator were established following the ISO 80601 and 80601 requirements. 
Such requirements are simplified and laid out in the MHRA rapid ventilator specifications.
The team working on this ventilator however decided to stick with the ISO 80601 specifications. Indeed, the MHRA document consists in streamlined regulations based off the ISO but made absolutely barebones to help speed things up. Since we don't know how long these regulations will apply, we thought it would make more sense to conform to the absolute gold standard which is ISO.

If a machine doesn’t meet those requirements it is likely to provide no clinical benefits, or worse, to be harmful to the patient.

Both the ISO 80601 and ISO 60601 as well as the MHRA documents can be downloaded from this folder.
