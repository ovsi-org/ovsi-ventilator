# Market study and inclusive innovation

This folder is dedicated to the nn-technical research around how OVSI could fit into the need of low- and middle-income countries (LMICs). This work relies on bibliographical research, market studies and interviews of clinitians and health experts in LMICs.

## Summary of Context-Appropriate Ventilator Design Factors
In this document, we summarize the context-dependent barriers for the design and deployment of ventilators in LMIC settings based on literature review. We also summarize potential design- and support-based solutions that can be implemented when deploying ventilators in these settings.

## Ventilator Technical Specs Comparison
In this excel file, we compiled various commercial and non-commercial ventilators that are serving (or can potentially serve) the LMIC market. For select ventilators, we also conducted a comprehensive comparison of their technical specifications and any after-sales support where available.
