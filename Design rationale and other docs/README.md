# Design rationale and other docs
This directory is filled with misc. documents which oriented the ventilator design to the ststae it is in now. Some are under-exploited and should be made use of to guide further iterations of the design.

The White paper describes the current mechanical components of the system.
The FMEA states the possible failure modes of each components. The content of the FMEA is dispatched in each component's directory under Design>components>individual_component>Risks&Failures

The sterilisation paper describes possible improvement avenues to make the device sterilizable.
