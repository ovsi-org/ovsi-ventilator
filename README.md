# OVSI Ventilator

### The OVSI ventilator does not have regulatory approval in any jurisdiction. It is the responsibility of licensees to comply with all applicable laws in relation to their use of the designs including without limitation obtaining applicable regulatory approval for medical devices manufactured to the designs in all relevant jurisdictions 

### This project is release under the terms of the CERN Open Hardware Licence version 2 (CERN-OHL-S). Further information can be found in the license file in the root folder of this project as well as the license folder. The disclaimer above applies in addition to the license.

### DO NOT BUILD THIS YOURSELF

### The OVSI ventilator is currently in an early prototype stage. It is never safe to build your own medical equipment. This project is open to encourage experts to work together to build the best and most adaptable solution. 

The Open Ventilator System Initiative (OVSI) is a project spearheaded by the Whittle Lab of the University of Cambridge. This project was initially started as a response to the COVID-19 pandemic and the shortages in critical medical equipment. This initiative consists in an open source and context appropriate Ventilator and Oxygen concentrator. **This gitlab documents the ventilator side of OVSI** 

## OVSI added value in the Ventilator landscape
thes OVSI ventilator is designed to be able to react to the patient input and switch between different ventilation modes: it can switch to mandatory breathing mode if the patient stops breathing on their own.

OVSI is also designed to fit the need of Low and Middle Income Countries (LMIC). More information about the context-appropriateness of OVSI and how it fits in the emergency response Ventilator lanscape [here](LINK)

More information on the design of OVSI [here](https://gitlab.com/ovsi-org/ovsi-ventilator/-/tree/master/Design)

## [The prototype Design](Design)

See the [Design folder](Design) for more details on the prototype design.
![](Assets/Images/flowchart.PNG)
![](Assets/Images/whittle2diagram.PNG)

### The Whittle Prototype

The ventilator has been rapidly prototyped at the Whittle laboratory in Cambridge. Many of the parts used in the prototype will be replaced with off-the-shelf-components or will be manufactured in a different way for a production model.

![](fullW2_picture.PNG)

## More info on ventilaors
this link https://www.ncbi.nlm.nih.gov/books/NBK448186/ and we should probably make an inclusive innovation folder that explains our constraints for design + hpotheses
